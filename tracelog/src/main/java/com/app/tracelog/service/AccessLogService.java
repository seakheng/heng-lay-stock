
package com.app.tracelog.service;

import com.app.core.service.CrudService;
import com.app.core.service.PagingNoCountService;
import com.app.tracelog.model.AccessLog;

public interface AccessLogService extends CrudService<AccessLog>, PagingNoCountService<AccessLog, AccessLog>  {
	
}
