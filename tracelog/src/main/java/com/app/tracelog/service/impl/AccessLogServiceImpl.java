package com.app.tracelog.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.app.tracelog.model.AccessLog;
import com.app.tracelog.repository.AccessLogRepository;
import com.app.tracelog.service.AccessLogService;

@Service("accessLogService")
public class AccessLogServiceImpl implements AccessLogService {

	@Autowired
	AccessLogRepository repo;
	
	@Override
	public AccessLog save(AccessLog model) {
		return repo.save(model);
	}

	@Override
	public void delete(AccessLog model) {
		// Auto-generated method stub
	}

	@Override
	public List<AccessLog> findAllNoCount(AccessLog dto, PageRequest pageRequest) {
		// Auto-generated method stub
		return new ArrayList<>();
	}

	@Override
	public long count(AccessLog dto) {
		// Auto-generated method stub
		return 0;
	}

	@Override
	public Page<AccessLog> findAll(AccessLog object, PageRequest pageRequest) {
		// Auto-generated method stub
		return null;
	}

}
