package com.app.tracelog.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.app.core.repository.NoCountPagingRepository;
import com.app.tracelog.model.AccessLog;

@Repository
public interface AccessLogRepository extends NoCountPagingRepository<AccessLog, Long>, JpaSpecificationExecutor<AccessLog> {
	
}
