package com.app.tracelog.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "SYS_ACCESS_LOG")
@EntityListeners(AuditingEntityListener.class)
public class AccessLog implements Serializable  {

	private static final long serialVersionUID = -5731653906843130875L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	@Column(name = "ID")
	private Long id;

	@Getter @Setter
	@Column(name = "REQUEST_ID")
	private String requestId;

	@Getter @Setter
	@Column(name = "COMMAND_NAME")
	private String commandName;

	@Getter @Setter
	@Column(name = "CLIENT_START_AT")
	private Date startAtClient;

	@Getter @Setter
	@Column(name = "SERVER_RECIEVE_AT")
	private Date recieveAtServer;

	@Getter @Setter
	@Column(name = "SERVER_COMPLETE_AT")
	private Date completeAtServer;

	@Getter @Setter
	@Column(name = "USER_IP")
	private String userIp;

	@Getter @Setter
	@Column(name = "USER_AGENT")
	private String userAgent;

	@Getter @Setter
	@Column(name = "USER_ID")
	private String userId;

	@Getter @Setter
	@OneToMany(mappedBy = "accessLog", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
	@OrderBy("id")
	private Set<AccessLogData> data;

	@Column(name = "created_on", nullable = false, updatable = false)
	@CreatedDate
	@Getter @Setter
	private Date createdOn;

	@Column(name = "created_by", nullable = false, updatable = false)
	@CreatedBy
	@Getter @Setter
	private String createdBy;

	public AccessLog() {}
	public static AccessLog create(
			String requestId,
			String commandName,
			Date startAtClient,
			Date recieveAtServer,
			Date completeAtServer,
			String userIp,
			String userAgent,
			String userId) {
		AccessLog accessLog = new AccessLog();
		accessLog.setRequestId(requestId);
		accessLog.setCommandName(commandName);
		accessLog.setStartAtClient(startAtClient);
		accessLog.setRecieveAtServer(recieveAtServer);
		accessLog.setCompleteAtServer(completeAtServer);
		accessLog.setUserIp(userIp);
		accessLog.setUserAgent(userAgent);
		accessLog.setUserId(userId);
		return accessLog;
	}

	public void addData(AccessLogData accessLogData) {
		if(accessLogData == null) return;
		if(data == null) {
			this.data = new TreeSet<>();
		}
		accessLogData.setAccessLog(this);
		this.data.add(accessLogData);
	}
}
