package com.app.tracelog.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "SYS_ACCESS_LOG_DATA")
public class AccessLogData implements Serializable, Comparable<AccessLogData> {
	private static final long serialVersionUID = 2093584298585141161L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;
	
	@Getter @Setter
    @Column(name = "NAME")
    private String name;
	
	@Getter @Setter
    @Column(name = "VALUE")
    private String value;
	
	@JoinColumn(name = "ACCESS_LOG_ID")
    @Getter @Setter
    @ManyToOne(fetch = FetchType.LAZY)
	private AccessLog accessLog;
	
	public static AccessLogData create(String name, String value) {
		AccessLogData ald = new AccessLogData();
		ald.setName(name);
		ald.setValue(value);
		return ald;
	}

	@Override
	public int compareTo(AccessLogData o) {
		if(getId() == null) return -1;
		return this.getId().compareTo(o.getId());

	}

}
