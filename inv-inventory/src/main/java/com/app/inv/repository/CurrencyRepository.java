package com.app.inv.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.inv.model.Currency;

@Repository
public interface CurrencyRepository extends CrudRepository<Currency, Long>, JpaSpecificationExecutor<Currency>{
	List<Currency> findByStatus(String status);
}
