package com.app.inv.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.model.BaseCrud;

@Entity
@Table(name = "inv_ccy")
@EntityListeners(AuditingEntityListener.class)
public class Currency extends BaseCrud{
	private static final long serialVersionUID = -1701911228958749597L;
}
