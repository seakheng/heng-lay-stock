package com.app.inv.service;

import java.util.List;

import com.app.core.service.CrudPagingService;
import com.app.inv.bo.CategoryParam;
import com.app.inv.model.Category;

public interface CategoryService extends CrudPagingService<Category, CategoryParam>{
	List<Category> findByStatus(String status);
}
