package com.app.inv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.inv.bo.CurrencyParam;
import com.app.inv.model.Currency;
import com.app.inv.repository.CurrencyRepository;
import com.app.inv.service.CurrencyService;

@Service("currencyService")
public class CurrencyServiceImpl implements CurrencyService{

	@Autowired
	private CurrencyRepository repo;

	@Override
	public Currency save(Currency model) {
		return repo.save(model);
	}

	@Override
	public void delete(Currency model) {
		repo.delete(model);
	}

	@Override
	public Page<Currency> findAll(CurrencyParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<Currency> spec(CurrencyParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%"+param.getName().toLowerCase()+"%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<Currency> findByStatus(String status) {
		return repo.findByStatus(status);
	}

}
