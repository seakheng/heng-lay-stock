package com.app.inv.service;

import java.util.List;

import com.app.core.service.CrudPagingService;
import com.app.inv.bo.CurrencyParam;
import com.app.inv.model.Currency;

public interface CurrencyService extends CrudPagingService<Currency, CurrencyParam>{
	List<Currency> findByStatus(String status);
}
