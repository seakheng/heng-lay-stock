package com.app.inv.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.inv.bo.CategoryParam;
import com.app.inv.bo.CurrencyParam;
import com.app.inv.model.Category;
import com.app.inv.model.Currency;
import com.app.inv.repository.CategoryRepository;
import com.app.inv.service.CategoryService;

@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{
	
	@Autowired
	CategoryRepository repo;
	

	@Override
	public Category save(Category model) {
		return repo.save(model);
	}

	@Override
	public void delete(Category model) {
		repo.delete(model);
	}

	@Override
	public Page<Category> findAll(CategoryParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}
	
	private Specification<Category> spec(CategoryParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%"+param.getName().toLowerCase()+"%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<Category> findByStatus(String status) {
		return repo.findByStatus(status);
	}

}
