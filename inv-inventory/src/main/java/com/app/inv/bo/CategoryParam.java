package com.app.inv.bo;

import lombok.Data;

@Data
public class CategoryParam {	
	private String name;
}
