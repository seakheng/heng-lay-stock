package com.app.dev.fixturescripts;

import javax.annotation.Priority;
import javax.script.ScriptException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.app.sample.model.Sample;
import com.app.sample.service.SampleService;

import lombok.extern.slf4j.Slf4j;

@Component
@Profile({ "h2dev", "test" })
@Priority(1)
@Slf4j
public class SampleFixtures implements ApplicationRunner {

	@Autowired
	private SampleService sampleService;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		for (int i = 1; i <= 5; i++) {
			Sample sample = Sample.create("Sample " + i, "Sample created form fixture scripts");
			sampleService.save(sample);
		}
		testScript();
	}

	public void testScript() throws ScriptException {
		javax.script.ScriptEngineManager scm = new javax.script.ScriptEngineManager();
		javax.script.ScriptEngine jsEngine = scm.getEngineByName("JavaScript");

		var result = jsEngine.eval("Math.abs(10 + 20 + 13 * 3/24)");
		log.info("Script Result: {}", result.toString());
	}

}
