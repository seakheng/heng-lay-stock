package com.app.dev.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.core.crypto.Crypto;
import com.app.security.bo.UserParam;
import com.app.security.model.Function;
import com.app.security.model.SimplePasswordEncoder;
import com.app.security.model.User;
import com.app.security.model.UserPrincipal;
import com.app.security.repository.UserRepository;
import com.app.security.service.UserService;

@Service("userService")
@Profile({ "h2dev", "test" })
public class UserServiceImpl implements UserService{

	@Value("${default.password}")
	private String defaultPassword;

	@Autowired
	private UserRepository repo;

	@Autowired
	Crypto crypto;

	private Map<String, User> getUsers() {
		var users = new TreeMap<String, User>();
		var user1 = User.createUser("anonymous.user", "", "Anonymous User");
		user1.setId(1L);

		var home = Function.create("home", "dashboard.zul", "Home", "Home Page", 1, 1, "home", "Y", null, "Y", "Y", "Y", "Y", "Y");

		// Administration
		var admin = Function.create("admin", "", "Administration", "Administration", 1, 2, "gear", "Y", null, "Y", "Y", "Y", "Y", "Y");
		var user = Function.create("user", "admin/user.zul", "User", "User", 2, 1, "users", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		var func = Function.create("function", "admin/function.zul", "Function", "Function", 2, 2, "gear", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		var role = Function.create("role", "admin/role.zul", "Role", "Role", 2, 3, "gear", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		var roleFunc = Function.create("access-right", "admin/role-func.zul", "Access Rights", "Access Rights", 2, 4, "gear", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		var config = Function.create("config-property", "admin/config-property.zul", "Config Property", "Config Property", 2, 5, "gear", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		var brn = Function.create("branch", "admin/branch.zul", "Branch", "Branch", 2, 6, "gear", "Y", admin, "Y", "Y", "Y", "Y", "Y");
		
		// Setting
		var setting = Function.create("setting", "", "Setting", "Setting", 1, 3, "gear", "Y", null, "Y", "Y", "Y", "Y", "Y");
		var ccy = Function.create("ccy", "inventory/setting/currency.zul", "Currency", "Currency", 2, 1, "gear", "Y", setting, "Y", "Y", "Y", "Y", "Y");
		var cate = Function.create("cate", "inventory/setting/category.zul", "Category", "Category", 2, 2, "gear", "Y", setting, "Y", "Y", "Y", "Y", "Y");
		
		user1.addFunction(home);
		user1.addFunction(admin);
		user1.addFunction(user);
		user1.addFunction(func);
		user1.addFunction(role);
		user1.addFunction(roleFunc);
		user1.addFunction(config);
		user1.addFunction(brn);
		user1.addFunction(setting);
		user1.addFunction(ccy);
		user1.addFunction(cate);
		users.put(user1.getUsername(), user1);

		return users;
	}

	@Override
	public UserDetails loadUser(String username, String password) {
		User user = getUsers().get(username);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new UserPrincipal(user);
	}

	@Override
	public PasswordEncoder encoder() {
		return new SimplePasswordEncoder();
	}

	@Override
	public User findByUsername(String username) {
		// TODO Auto-generated method stub
		return getUsers().get("anonymous.user");
	}

	@Override
	public User findByUsernameAndStatus(String username, String status) {
		// TODO Auto-generated method stub
		return getUsers().get("anonymous.user");
	}

	@Override
	public List<Function> getUserFunction(User user) {
		// TODO Auto-generated method stub
		return getUsers().get("anonymous.user").getFunctions();
	}

	@Override
	public User save(User model) {
		if (StringUtils.isEmpty(model.getPassword())) {
			model.setPassword(crypto.encryptWithBase64(defaultPassword));
		}
		return repo.save(model);
	}

	@Override
	public void delete(User model) {
		repo.delete(model);
	}

	@Override
	public Page<User> findAll(UserParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<User> spec(UserParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(param.getEmployeeId())) {
				predicates.add(cb.like(cb.lower(root.get("employeeId")), "%"+param.getEmployeeId().toLowerCase()+"%"));
			}
			if(!StringUtils.isEmpty(param.getUsername())) {
				predicates.add(cb.like(cb.lower(root.get("username")), "%"+param.getUsername().toLowerCase()+"%"));
			}

			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public void changePassword(Long id, String newPassword) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<User> findByStatus(String status) {
		// TODO Auto-generated method stub
		return repo.findByStatus(status);
	}

	@Override
	public boolean isGranted(String username, String func, String per) {
		// TODO Auto-generated method stub
		return true;
	}
}