package com.app.security.bo;

import lombok.Data;

@Data
public class RoleParam {
	private String name;
}
