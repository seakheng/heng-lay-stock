package com.app.security.bo;

import com.app.security.model.Function;
import com.app.security.model.Role;

import lombok.Data;

@Data
public class RoleFuncParam {
	private Role role;
	private Function function;
}
