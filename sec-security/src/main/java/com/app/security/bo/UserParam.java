package com.app.security.bo;

import lombok.Data;

@Data
public class UserParam {
	private String employeeId;
	private String username;
}
