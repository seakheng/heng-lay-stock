package com.app.security.bo;

import com.app.security.model.Function;

import lombok.Data;

@Data
public class FunctionParam {
	private String name;
	private Function parent;
}
