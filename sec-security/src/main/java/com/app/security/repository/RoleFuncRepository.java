package com.app.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;

@Repository
public interface RoleFuncRepository extends CrudRepository<RoleFunction, Long>, JpaSpecificationExecutor<RoleFunction>{
	List<RoleFunction> findByRoleAndStatus(Role role, String status);
	List<RoleFunction> findByRoleInAndStatus(List<Role> roles, String status);
	List<RoleFunction> findByRoleInAndFunctionAndStatus(List<Role> roles, Function func, String status);
}
