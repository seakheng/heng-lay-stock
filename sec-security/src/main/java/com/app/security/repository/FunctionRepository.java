package com.app.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.security.model.Function;

@Repository
public interface FunctionRepository extends CrudRepository<Function, Long>, JpaSpecificationExecutor<Function>{
	List<Function> findByStatus(String status);
	List<Function> findByCodeNotInAndStatus(List<String> code, String status);
	Function findByCodeAndStatus(String code, String status);
}
