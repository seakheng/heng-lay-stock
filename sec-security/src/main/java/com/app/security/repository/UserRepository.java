package com.app.security.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.security.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Long>, JpaSpecificationExecutor<User>{
	List<User> findByStatus(String status);
	User findByUsername(String username);
	User findByUsernameAndStatus(String username, String status);
}
