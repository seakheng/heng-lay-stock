package com.app.security.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.model.BaseCrud;

@Entity(name = "sys_role")
@EntityListeners(AuditingEntityListener.class)
public class Role extends BaseCrud{
	private static final long serialVersionUID = 363239553254009383L;

	public static Role create(String code, String desc, String name) {
		Role role = new Role();
		role.setCode(code);
		role.setDesc(desc);
		role.setName(name);
		return role;
	}

	@Override
	public int hashCode() {
		return Objects.hash(getCode());
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (!(other instanceof Role))
			return false;
		Role role = (Role) other;
		return Objects.equals(role.getCode(), this.getCode());
	}

	@Override
	public String toString() {
		return String.format("%s - %s", getCode(), getName());
	}
}
