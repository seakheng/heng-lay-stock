package com.app.security.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.model.Auditable;
import com.app.core.model.Branch;
import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Entity
@Table(name = "sys_user")
@EntityListeners(AuditingEntityListener.class)
public class User extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = -5559170840427096285L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(name = "full_name", length = 100, nullable = false)
	@Size(min = 1, max = 100, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String fullName;

	@Getter
	@Setter
	@Column(unique = true, name = "username", length = 100, nullable = false)
	@Size(min = 1, max = 100, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String username;

	@Getter
	@Setter
	@Column(unique = true, name = "email", length = 255, nullable = false)
	@Email(message = ValidationMessage.EMAIL)
	@Size(min = 1, max = 255, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String email;

	@Getter
	@Setter
	@Column(name = "password", length = 255, nullable = false)
	private String password;

	@Getter
	@Setter
	@Column(name = "employee_id", length = 30, nullable = false)
	@Size(min = 1, max = 30, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String employeeId;
	
	@Getter
	@Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "home_branch", referencedColumnName = "code", nullable = false)
	@NotNull(message = ValidationMessage.NOT_NULL)
	private Branch branch;

	@Getter
	@Setter
	@Column(name = "grant_type", length = 10, nullable = false)
	@Size(min = 1, max = 10, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String grantType;

	@Getter
	@Setter
	@Column(name = "force_change_pwd", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String forceChangePwd = "Y";

	@Getter
	@Setter
	@Column(name = "last_login_on")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLoginOn;

	@Getter
	@Setter
	@ManyToMany(fetch = FetchType.EAGER)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JoinTable(name = "sys_user_role", joinColumns = {
			@JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "role_code", referencedColumnName = "code", nullable = false) })
	private List<Role> roles = new ArrayList<Role>();

	@Getter
	@Setter
	@Transient
	private List<Function> functions = new ArrayList<Function>();

	public static User createUser(@NonNull final String username, @NonNull final String password,
			@NonNull final String fullName) {
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setFullName(fullName);
		return user;
	}

	public void addFunction(Function func) {
		this.functions.add(func);
	}

	@Override
	public String toString() {
		return String.format("%s", this.fullName);
	}
	
	
}