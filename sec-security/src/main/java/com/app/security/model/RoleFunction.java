package com.app.security.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.app.core.model.Auditable;
import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sys_role_function", uniqueConstraints = @UniqueConstraint(columnNames = {"role_code", "function_code"}))
public class RoleFunction extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 9033649703465145353L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "role_code", referencedColumnName = "code", nullable = false)
	@NotNull(message = ValidationMessage.NOT_NULL)
	private Role role;

	@Getter
	@Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "function_code", referencedColumnName = "code", nullable = false)
	@NotNull(message = ValidationMessage.NOT_NULL)
	private Function function;

	@Getter
	@Setter
	@Column(name = "c", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String c;

	@Getter
	@Setter
	@Column(name = "r", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String r;

	@Getter
	@Setter
	@Column(name = "u", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String u;

	@Getter
	@Setter
	@Column(name = "d", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String d;

	@Getter
	@Setter
	@Column(name = "x", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String x;

	public RoleFunction() {
	}

	public RoleFunction(Role role, Function function) {
		super();
		this.role = role;
		this.function = function;
	}
}
