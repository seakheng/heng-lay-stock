package com.app.security.model;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.model.Auditable;
import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sys_function")
@EntityListeners(AuditingEntityListener.class)
public class Function extends Auditable<String> implements Serializable, Cloneable {
	private static final long serialVersionUID = -3542255936975573104L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(unique = true, name = "code", length = 30, nullable = false)
	@Size(min = 1, max = 30, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String code;

	@Getter
	@Setter
	@Column(name = "name", length = 100, nullable = false)
	@Size(min = 1, max = 100, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String name;

	@Getter
	@Setter
	@Column(name = "description", length = 255)
	@Size(max = 255, message = ValidationMessage.MAX)
	private String desc;

	@Getter
	@Setter
	@Column(name = "path", length = 100)
	@Size(max = 100, message = ValidationMessage.MAX)
	private String path;

	@Getter
	@Setter
	@Column(name = "level")
	private Integer level;

	@Getter
	@Setter
	@Column(name = "sequence")
	private Integer sequence;

	@Getter
	@Setter
	@Column(name = "icon", length = 30)
	@Size(max = 30, message = ValidationMessage.MAX)
	private String icon;

	@Getter
	@Setter
	@Column(name = "menu", length = 1)
	@Size(max = 1, message = ValidationMessage.MAX)
	private String menu;

	@Getter
	@Setter
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent", referencedColumnName = "code", nullable = true)
	private Function parent;

	@Getter
	@Setter
	@Transient
	private String c;

	@Getter
	@Setter
	@Transient
	private String r;

	@Getter
	@Setter
	@Transient
	private String u;

	@Getter
	@Setter
	@Transient
	private String d;

	@Getter
	@Setter
	@Transient
	private String x;

	public Function() {
	}

	public static Function create(String code, String path, String name, String desc, Integer level, Integer sequence,
			String icon, String menu, Function parent, String c, String r, String u, String d, String x) {
		Function func = new Function();
		func.setCode(code);
		func.setPath(path);
		func.setName(name);
		func.setDesc(desc);
		func.setLevel(level);
		func.setSequence(sequence);
		func.setIcon(icon);
		func.setMenu(menu);
		func.setParent(parent);
		func.setC(c);
		func.setR(r);
		func.setU(u);
		func.setD(d);
		func.setX(x);
		return func;
	}

	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	@Override
	public int hashCode() {
		return Objects.hash(code);
	}

	@Override
	public boolean equals(Object other) {
		if (this == other)
			return true;
		if (!(other instanceof Function))
			return false;
		Function func = (Function) other;
		return Objects.equals(func.getCode(), this.getCode());
	}
	
	@Override
	public String toString() {
		if(this.getParent() != null)
			return String.format("%s - %s", this.parent.getName(), this.name);
		else
			return this.name;
	}
}
