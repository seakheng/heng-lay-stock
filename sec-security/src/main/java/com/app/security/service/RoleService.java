package com.app.security.service;

import java.util.List;

import com.app.core.service.CrudPagingService;
import com.app.security.bo.RoleParam;
import com.app.security.model.Role;

public interface RoleService extends CrudPagingService<Role, RoleParam>{
	List<Role> findByStatus(String status);
}
