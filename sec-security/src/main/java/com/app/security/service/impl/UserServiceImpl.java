package com.app.security.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.core.constant.Constant;
import com.app.core.constant.StatusEnum;
import com.app.core.crypto.Crypto;
import com.app.security.bo.UserParam;
import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;
import com.app.security.model.SimplePasswordEncoder;
import com.app.security.model.User;
import com.app.security.model.UserPrincipal;
import com.app.security.repository.UserRepository;
import com.app.security.service.FunctionService;
import com.app.security.service.LdapService;
import com.app.security.service.RoleFuncService;
import com.app.security.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Service("userService")
@Profile({ "prd", "dev", "staging", "uat" })
@Slf4j
public class UserServiceImpl implements UserService {

	@Value("${default.password:#{null}}")
	private String defaultPassword;

	@Autowired
	private UserRepository repo;

	@Autowired
	private RoleFuncService roleFuncService;

	@Autowired
	private FunctionService functionService;

	@Autowired
	private LdapService ldapService;

	@Autowired
	Crypto crypto;

	@Override
	public UserDetails loadUser(String username, String password) {
		User user = findUser(username, password);
		if (user == null) {
			throw new UsernameNotFoundException(username);
		}
		return new UserPrincipal(user);
	}

	public PasswordEncoder encoder() {
		return new SimplePasswordEncoder();
	}

	private User findUser(String username, String password) {
		User user = repo.findByUsername(username);

		if (user == null) {
			log.error("Username {} not found", username);
		} else {
			if (user.isSuspended()) {
				log.error("Username {} is suspended", username);
				return null;
			} else if (user.isLocked()) {
				log.error("Username {} is locked", username);
				return null;
			} else if (user.isBlocked()) {
				log.error("Username {} is blocked", username);
				return null;
			} else if (user.isDisabled()) {
				log.error("Username {} is disabled", username);
				return null;
			}

			boolean authenticated = true;
			if (user.getGrantType().equals(Constant.GRANT_TYPE_LDAP)) {
				log.info("Authenticate username {} with LDAP", username);
				authenticated = ldapService.authenticateUser(username, password);
			} else {
				log.info("Authenticate username {} within APP", username);
				String appPassword = crypto.encryptWithBase64(password);
				authenticated = user.getPassword().equals(appPassword);
			}

			if (!authenticated) {
				log.error("Username and password is incorrect");
				return null;
			} else {
				log.info("User login successfully");
				// save last login
				User lastLogin = repo.findById(user.getId()).get();
				lastLogin.setLastLoginOn(new Date());
				repo.save(lastLogin);
			}
		}

		return user;
	}

	@Override
	public User findByUsername(String username) {
		return repo.findByUsername(username);
	}

	@Override
	public User findByUsernameAndStatus(String username, String status) {
		return repo.findByUsernameAndStatus(username, status);
	}

	@Override
	public List<Function> getUserFunction(User user) {
		try {
			List<Function> res = new ArrayList<Function>();
			List<Role> roles = new ArrayList<Role>();

			for (int i = 0; i < user.getRoles().size(); i++) {
				if (user.getRoles().get(i).isActive()) {
					roles.add(user.getRoles().get(i));
				}
			}

			List<RoleFunction> roleFunc = roleFuncService.findByRoleInAndStatus(roles,
					StatusEnum.STATUS_ACTIVE.getCode());
			for (int i = 0; i < roleFunc.size(); i++) {
				Function func = (Function) roleFunc.get(i).getFunction().clone();
				if (func.isActive()) {
					func.setC(roleFunc.get(i).getC());
					func.setR(roleFunc.get(i).getR());
					func.setU(roleFunc.get(i).getU());
					func.setD(roleFunc.get(i).getD());
					func.setX(roleFunc.get(i).getX());
					res.add(func);
				}
			}
			return res;
		} catch (Exception e) {
			log.error("Error while get user function", e);
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public User save(User model) {
		if (StringUtils.isEmpty(model.getPassword()))
			model.setPassword(crypto.encryptWithBase64(defaultPassword));
		if (Constant.GRANT_TYPE_LDAP.equals(model.getGrantType()))
			model.setForceChangePwd("N");

		return repo.save(model);
	}

	@Override
	public void delete(User model) {
		repo.delete(model);
	}

	@Override
	public Page<User> findAll(UserParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<User> spec(UserParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isEmpty(param.getEmployeeId())) {
				predicates.add(
						cb.like(cb.lower(root.get("employeeId")), "%" + param.getEmployeeId().toLowerCase() + "%"));
			}
			if (!StringUtils.isEmpty(param.getUsername())) {
				predicates.add(cb.like(cb.lower(root.get("username")), "%" + param.getUsername().toLowerCase() + "%"));
			}

			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public void changePassword(Long id, String newPassword) {
		User user = repo.findById(id).get();
		user.setPassword(crypto.encryptWithBase64(newPassword));
		user.setForceChangePwd("N");
		repo.save(user);
	}

	@Override
	public List<User> findByStatus(String status) {
		return repo.findByStatus(status);
	}

	@Override
	public boolean isGranted(String username, String func, String per) {
		User user = findByUsername(username);
		Function function = functionService.findByCodeAndStatus(func, StatusEnum.STATUS_ACTIVE.getCode());
		List<Role> roles = new ArrayList<Role>();

		for (int i = 0; i < user.getRoles().size(); i++) {
			if (user.getRoles().get(i).isActive()) {
				roles.add(user.getRoles().get(i));
			}
		}

		List<RoleFunction> rf = roleFuncService.findByRoleInAndFunctionAndStatus(roles, function, StatusEnum.STATUS_ACTIVE.getCode());
		for (int i = 0; i < rf.size(); i++) {
			if ("C".equals(per) && "Y".equals(rf.get(i).getC())) return true;
			if ("R".equals(per) && "Y".equals(rf.get(i).getR())) return true;
			if ("U".equals(per) && "Y".equals(rf.get(i).getU())) return true;
			if ("D".equals(per) && "Y".equals(rf.get(i).getD())) return true;
			if ("X".equals(per) && "Y".equals(rf.get(i).getX())) return true;
		}

		return false;
	}
}
