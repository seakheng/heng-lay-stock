package com.app.security.service.impl;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.directory.InitialDirContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.app.security.service.LdapService;

import lombok.extern.slf4j.Slf4j;

@Service("ldapService")
@Slf4j
public class LdapServiceImpl implements LdapService {
	private static final String INITIAL_CONTEXT_FACTORY = "com.sun.jndi.ldap.LdapCtxFactory";
	private static final String SECURITY_AUTHENTICATION = "simple";

	@Value("${ldap.url:#{null}}")
	private String ldapUrl;
	@Value("${ldap.domain:#{null}}")
	private String ldapDomain;
	@Value("${ldap.port:#{null}}")
	private String ldapPort;

	@Override
	public boolean authenticateUser(String username, String password) {
		log.info("Authenticate LDAP user {}", username);
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, INITIAL_CONTEXT_FACTORY);
		env.put(Context.SECURITY_AUTHENTICATION, SECURITY_AUTHENTICATION);
		env.put(Context.PROVIDER_URL, "ldap://" + ldapUrl + ":" + ldapPort);
		env.put(Context.SECURITY_PRINCIPAL, username + "@" + ldapDomain);
		env.put(Context.SECURITY_CREDENTIALS, password);

		try {
			new InitialDirContext(env); // throw exception, if username-password not correct
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			log.error("Error while login with AD", e);
			return false;
		}
	}
}
