package com.app.security.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.app.core.service.CrudPagingService;
import com.app.security.bo.UserParam;
import com.app.security.model.Function;
import com.app.security.model.User;

public interface UserService extends CrudPagingService<User, UserParam> {
	public UserDetails loadUser(String username, String password);

	public PasswordEncoder encoder();
	
	public List<User> findByStatus(String status);

	public User findByUsername(String username);

	public User findByUsernameAndStatus(String username, String status);

	public List<Function> getUserFunction(User user);
	
	public void changePassword(Long id, String newPassword);
	
	public boolean isGranted(String username, String func, String per);
}
