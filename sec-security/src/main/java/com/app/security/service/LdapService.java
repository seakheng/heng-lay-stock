package com.app.security.service;

public interface LdapService {
	public boolean authenticateUser(String username, String password);
}
