package com.app.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.app.security.bo.RoleFuncParam;
import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;
import com.app.security.repository.RoleFuncRepository;
import com.app.security.service.RoleFuncService;

@Service("roleFuncService")
public class RoleFuncServiceImpl implements RoleFuncService{

	@Autowired
	private RoleFuncRepository repo;

	@Override
	public List<RoleFunction> findByRoleAndStatus(Role role, String status) {
		return repo.findByRoleAndStatus(role, status);
	}

	@Override
	public List<RoleFunction> findByRoleInAndStatus(List<Role> roles, String status) {
		return repo.findByRoleInAndStatus(roles, status);
	}

	@Override
	public RoleFunction save(RoleFunction model) {
		return repo.save(model);
	}

	@Override
	public void delete(RoleFunction model) {
		repo.delete(model);
	}

	@Override
	public Page<RoleFunction> findAll(RoleFuncParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}


	private Specification<RoleFunction> spec(RoleFuncParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (param.getRole() != null) {
				predicates.add(cb.equal(root.get("role"), param.getRole()));
			}
			if (param.getFunction() != null) {
				predicates.add(cb.equal(root.get("function"), param.getFunction()));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<RoleFunction> findByRoleInAndFunctionAndStatus(List<Role> roles, Function func, String status) {
		return repo.findByRoleInAndFunctionAndStatus(roles, func, status);
	}
}
