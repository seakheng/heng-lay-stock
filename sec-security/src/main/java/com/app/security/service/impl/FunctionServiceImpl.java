package com.app.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.core.constant.StatusEnum;
import com.app.security.bo.FunctionParam;
import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;
import com.app.security.repository.FunctionRepository;
import com.app.security.service.FunctionService;
import com.app.security.service.RoleFuncService;

@Service("functionService")
public class FunctionServiceImpl implements FunctionService {

	@Autowired
	private FunctionRepository repo;

	@Autowired
	private RoleFuncService roleFuncService;

	@Override
	public Function save(Function model) {
		return repo.save(model);
	}

	@Override
	public void delete(Function model) {
		repo.delete(model);
	}

	@Override
	public Page<Function> findAll(FunctionParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<Function> spec(FunctionParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%" + param.getName().toLowerCase() + "%"));
			}
			if (param.getParent() != null) {
				predicates.add(cb.equal(root.get("parent"), param.getParent()));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<Function> findByStatus(String status) {
		return repo.findByStatus(status);
	}

	@Override
	public List<Function> findByCodeNotInAndStatus(List<String> code, String status) {
		return repo.findByCodeNotInAndStatus(code, status);
	}

	@Override
	public List<Function> findAllNotInRole(Role role) {
		List<String> code = new ArrayList<String>();
		code.add("");
		List<RoleFunction> rf = roleFuncService.findByRoleAndStatus(role,
				StatusEnum.STATUS_ACTIVE.getCode());
		for (int i = 0; i < rf.size(); i++) {
			code.add(rf.get(i).getFunction().getCode());
		}
		return this.findByCodeNotInAndStatus(code, StatusEnum.STATUS_ACTIVE.getCode());
	}

	@Override
	public Function findByCodeAndStatus(String code, String status) {
		return repo.findByCodeAndStatus(code, status);
	}

}
