package com.app.security.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.security.bo.RoleParam;
import com.app.security.model.Role;
import com.app.security.repository.RoleRepository;
import com.app.security.service.RoleService;

@Service("roleService")
public class RoleServiceImpl implements RoleService{

	@Autowired
	private RoleRepository repo;

	@Override
	public Role save(Role model) {
		return repo.save(model);
	}

	@Override
	public void delete(Role model) {
		repo.delete(model);
	}

	@Override
	public Page<Role> findAll(RoleParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<Role> spec(RoleParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%"+param.getName().toLowerCase()+"%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<Role> findByStatus(String status) {
		return repo.findByStatus(status);
	}

}
