package com.app.security.service;

import java.util.List;

import com.app.core.service.CrudPagingService;
import com.app.security.bo.FunctionParam;
import com.app.security.model.Function;
import com.app.security.model.Role;

public interface FunctionService extends CrudPagingService<Function, FunctionParam> {
	List<Function> findByStatus(String status);
	List<Function> findByCodeNotInAndStatus(List<String> code, String status);
	List<Function> findAllNotInRole(Role role);
	Function findByCodeAndStatus(String code, String status);
}
