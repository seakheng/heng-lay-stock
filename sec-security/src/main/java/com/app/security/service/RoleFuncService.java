package com.app.security.service;

import java.util.List;

import com.app.core.service.CrudPagingService;
import com.app.security.bo.RoleFuncParam;
import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;

public interface RoleFuncService extends CrudPagingService<RoleFunction, RoleFuncParam>{
	List<RoleFunction> findByRoleAndStatus(Role role, String status);
	List<RoleFunction> findByRoleInAndStatus(List<Role> roles, String status);
	List<RoleFunction> findByRoleInAndFunctionAndStatus(List<Role> roles, Function func, String status);
}
