package com.app.core.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {

	public static String datetimeFormat(Date date) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		return df.format(date);
	}

	public static void configProperties(ApplicationContext content, String key, String value) {
		try {
			key = "${" + key;
			for (var n : content.getBeanDefinitionNames()) {
				Object bean = content.getBean(n);
				for (var f : bean.getClass().getDeclaredFields()) {
					if (f.isAnnotationPresent(Value.class)) {
						var v = f.getAnnotation(Value.class);
						if (v.value().startsWith(key)) {
							f.setAccessible(true);
							f.set(bean, value);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error while refresh config property", e);
		}
	}

	public static String getElapsedTime(Date d1, Date d2) {
		if (d2.compareTo(d1) >= 0) 
			return DurationFormatUtils.formatDuration(d2.getTime() - d1.getTime(), "HH:mm:ss");
		return "";
	}

	public static String readObjFieldExpr(String str, Object entity) {
		return Utils.readObjFieldExpr(str, entity, "\\$\\{(.*?)\\}");
	}

	public static String readObjFieldExpr(String str, Object entity, String rx) {
		try {
			Map<String, String> map = Utils.mapDeclareFields(entity);

			StringBuffer sb = new StringBuffer();
			Pattern p = Pattern.compile(rx);
			Matcher m = p.matcher(str);

			while (m.find()) {
				// Avoids throwing a NullPointerException in the case that you
				// Don't have a replacement defined in the map for the match
				String repString = (String) map.get(m.group(1));
				if (repString != null)
					m.appendReplacement(sb, repString);
			}
			m.appendTail(sb);

			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return str;
	}

	public static Map<String, String> mapDeclareFields(Object entity)
			throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
		Map<String, String> map = new HashMap<String, String>();

		Method[] methods = entity.getClass().getMethods();
		for (Method m : methods) {
			if (m.getName().startsWith("get")) {
				var name = m.getName();
				var obj = m.invoke(entity);
				if (obj != null) {
					if (Arrays.asList("java.util.Date", "java.sql.Timestamp").contains(obj.getClass().getTypeName()))
						obj = Utils.datetimeFormat((Date) obj);
				}
				map.put(name.substring(3, 4).toLowerCase() + name.substring(4), String.valueOf(obj));
			}
		}

		return map;
	}
}
