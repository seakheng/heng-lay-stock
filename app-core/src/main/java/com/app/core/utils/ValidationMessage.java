package com.app.core.utils;

public abstract class ValidationMessage {
	private ValidationMessage () {
		//private constructor
	}
	public static final String NOT_NULL = "validation.message.notnull";
	public static final String NOT_BLANK = "validation.message.notblank";
	public static final String NOT_EMPTY = "validation.message.notempty";
	public static final String SIZE = "validation.message.size";
	public static final String MIN = "validation.message.min";
	public static final String MAX = "validation.message.max";
	public static final String UNIQUE = "validation.message.unique";
	public static final String EMAIL = "validation.message.email";
}
