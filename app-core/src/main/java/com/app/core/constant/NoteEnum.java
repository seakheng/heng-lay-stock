package com.app.core.constant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

public enum NoteEnum {
	// #{} for old ticket
	// ${} for new ticket
	NOTE_INIT("INIT", "Init", "The ticket was generated on <b>${createdOn}</b>"),
	NOTE_CALL("CALL", "Call",
			"Called on <b>${calledOn}</b>, Time waiting <span class='badge bg-red'>${timeWaiting}</span>"),
	NOTE_START("START", "Start", "Start served on <b>${startServe}</b>"),
	NOTE_RECALL("RECALL", "Recall",
			"Recalled on <b>${recalledOn}</b>, Counter waiting <span class='badge bg-red'>${recalledWaiting}</span>"),
	NOTE_SKIP("SKIP", "Skip", "Skipped on <b>${skippedOn}</b>"),
	NOTE_STOP("STOP", "End",
			"End served on <b>${endServe}</b> , Time taken <span class='badge bg-red'>${timeTaken}</span>, Total time <span class='badge bg-red'>${totalTime}</span>"),
	NOTE_ADD_SERVICE("ADD_SERVICE", "Add Service", "Added service <b>${service}</b>"),
	NOTE_CHANGE_SERVICE("CHANGE_SERVICE", "Change Service",
			"Changed service from <b>#{service}</b> -> <b>${service}</b>"),
	NOTE_RESERVE("RESERVE", "Reserve", "Reserve from current state #{statusHtml} -> ${statusHtml}"),
	NOTE_TRANSFER("TRANSFER", "Transfer",
			"Transfer counter from <b>#{counter}</b> <b>##{service}</b> -> <b>${counter}</b> <b>#${service}</b>");

	@Getter
	private final String type;

	@Getter
	private final String name;

	@Getter
	private final String note;

	@Getter
	private static final Map<String, NoteEnum> byType = new HashMap<>();

	NoteEnum(String type, String name, String note) {
		this.type = type;
		this.name = name;
		this.note = note;
	}

	static {
		for (NoteEnum note : values()) {
			byType.put(note.getType(), note);
		}
	}

	public static List<NoteEnum> createLst() {
		return Arrays.asList(NoteEnum.values());
	}
}
