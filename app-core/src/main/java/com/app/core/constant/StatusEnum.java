package com.app.core.constant;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;

public enum StatusEnum {
	STATUS_ACTIVE("ACT", "Active", "bg-green"),
	STATUS_SUSPEND("SUS", "Suspend", "bg-gray"),
	STATUS_LOCKED("LCK", "Locked", "bg-yellow"),
	STATUS_BLOCK("BLK", "Block", "bg-gray"),
	STATUS_DISABLE("DBL", "Disable", "bg-yellow"),
	STATUS_DELETE("DEL", "Delete", "bg-red"),
	STATUS_CALLED("CAL", "Called", "label-primary"),
	STATUS_SERVED("SRV", "Served", "label-primary"),
	STATUS_RESERVED("RSRV", "Reserved", "label-primary"),
	STATUS_COMPLETED("CMP", "Completed", "bg-green"),
	STATUS_SKIPPED("SKI", "Skipped", "bg-yellow"),
	STATUS_TRANSFERRED("TRF", "Transferred", "label-primary"),
	STATUS_WAITING("WAT", "Waiting", "label-primary");

	@Getter
	private final String code;

	@Getter
	private final String name;

	@Getter
	private final String color;

	@Getter
	private static final Map<String, StatusEnum> byCode = new HashMap<>();

	StatusEnum(String code, String name, String color) {
		this.code = code;
		this.name = name;
		this.color = color;
	}

	static {
		for (StatusEnum status : values()) {
			byCode.put(status.getCode(), status);
		}
	}

	public static List<StatusEnum> getLst(){
		return Arrays.asList(StatusEnum.values());
	}
}
