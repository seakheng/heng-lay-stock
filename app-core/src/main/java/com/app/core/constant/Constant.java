package com.app.core.constant;

public interface Constant {
	public static final String GRANT_TYPE_LDAP = "ldap";
	public static final String SIGNAGE_ADDRESS_KEY = "app.signage.address";
	public static final Integer TRANSFER_GENERAL_COUNTER_NO = -1;
}
