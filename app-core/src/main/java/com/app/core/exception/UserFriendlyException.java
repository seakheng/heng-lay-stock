package com.app.core.exception;

public class UserFriendlyException extends Exception {
	private static final long serialVersionUID = 8385300329153501938L;
	
	public UserFriendlyException(String message) {
		super(message);
	}
}
