package com.app.core.crypto;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Crypto {

	@Value("${crypto.key:#{null}}")
	private String key;
	
	@Value("${crypto.initvector:#{null}}")
	private String initVector;

	public String encryptWithBase64(String value) {
		return this.encrypt(new String(Base64.decodeBase64(value)));
	}

	public String encrypt(String value) {
		return this.encrypt(key, initVector, value);
	}

	public String encrypt(String key, String initVector, String value) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

			byte[] encrypted = cipher.doFinal(value.getBytes());

			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	public String decryptWithBase64(String value) {
		return this.decrypt(Base64.encodeBase64String(value.getBytes()));
	}

	public String decrypt(String value) {
		return this.decrypt(key, initVector, value);
	}

	public String decrypt(String key, String initVector, String encrypted) {
		try {
			IvParameterSpec iv = new IvParameterSpec(initVector.getBytes("UTF-8"));
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

			byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

			return new String(original);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
	
	public boolean isEncrypted(String value) {
		return decrypt(value)!=null;
	}


//		public static void main(String[] args) { 
//			Crypto c = new Crypto();
//			c.key = "Bar12345Bar12345"; //128 bit key 
//			c.initVector = "RandomInitVector"; // 16 bytes IV 
//			
//			System.out.println(c.encryptWithBase64("HKL@9999")); 
//		}

}
