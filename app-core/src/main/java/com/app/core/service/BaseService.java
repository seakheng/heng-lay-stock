package com.app.core.service;

public interface BaseService<T, P> {
	T findById(Long id);
}
