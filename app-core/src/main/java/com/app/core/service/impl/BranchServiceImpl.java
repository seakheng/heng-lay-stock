package com.app.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.core.bo.BranchParam;
import com.app.core.model.Branch;
import com.app.core.repository.BranchRepository;
import com.app.core.service.BranchService;

@Service("branchService")
public class BranchServiceImpl implements BranchService{

	@Autowired
	private BranchRepository repo;

	@Override
	public Branch save(Branch model) {
		return repo.save(model);
	}

	@Override
	public void delete(Branch model) {
		repo.delete(model);
	}

	@Override
	public Page<Branch> findAll(BranchParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<Branch> spec(BranchParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%"+param.getName().toLowerCase()+"%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<Branch> findByStatus(String status) {
		return repo.findByStatus(status);
	}

	@Override
	public Branch findByCodeAndStatus(String code, String status) {
		return repo.findByCodeAndStatus(code, status);
	}

}
