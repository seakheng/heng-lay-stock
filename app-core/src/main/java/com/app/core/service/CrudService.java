package com.app.core.service;

public interface CrudService<T> {
	T save(T model);
	void delete(T model);
}
