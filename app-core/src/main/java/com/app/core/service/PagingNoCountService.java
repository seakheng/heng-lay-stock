package com.app.core.service;

import java.util.List;

import org.springframework.data.domain.PageRequest;

public interface PagingNoCountService<T, P> extends PagingService<T, P>{
	List<T> findAllNoCount(P param, PageRequest pageRequest);
	long count(P param);
}
