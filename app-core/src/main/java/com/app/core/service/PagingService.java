package com.app.core.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface PagingService<T, P> {
	Page<T> findAll(P param, PageRequest pageRequest);
}
