package com.app.core.service;

public interface CrudPagingService<T, P> extends CrudService<T>, PagingService<T, P>{
}
