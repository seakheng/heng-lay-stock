package com.app.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.core.bo.ConfigPropertyParam;
import com.app.core.constant.StatusEnum;
import com.app.core.model.ConfigProperty;
import com.app.core.repository.ConfigPropertyRepository;
import com.app.core.service.ConfigPropertyService;
import com.app.core.utils.Utils;

@Service("configPropertyService")
public class ConfigPropertyServiceImpl implements ConfigPropertyService {

	@Autowired
	private ConfigPropertyRepository repo;

	@Autowired
	private ApplicationContext appContext;

	@Override
	public ConfigProperty save(ConfigProperty model) {
		model = repo.save(model);
		Utils.configProperties(appContext, model.getKey(), model.getValue());
		return model;
	}

	@Override
	public void delete(ConfigProperty model) {
		repo.delete(model);
		Utils.configProperties(appContext, model.getKey(), "");
	}

	@Override
	public Page<ConfigProperty> findAll(ConfigPropertyParam param, PageRequest pageRequest) {
		return repo.findAll(this.spec(param), pageRequest);
	}

	private Specification<ConfigProperty> spec(ConfigPropertyParam param) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if (!StringUtils.isEmpty(param.getName())) {
				predicates.add(cb.like(cb.lower(root.get("name")), "%" + param.getName().toLowerCase() + "%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public List<ConfigProperty> findByStatus(String status) {
		return repo.findByStatus(status);
	}

	@Override
	public void refreshConfigProperty() {
		this.findByStatus(StatusEnum.STATUS_ACTIVE.getCode()).forEach(each -> {
			Utils.configProperties(appContext, each.getKey(), each.getValue());
		});
	}

	@Override
	public ConfigProperty findByKey(String key) {
		return repo.findByKey(key);
	}
}
