package com.app.core.service;

import java.util.List;

import com.app.core.bo.BranchParam;
import com.app.core.model.Branch;

public interface BranchService extends CrudPagingService<Branch, BranchParam>{
	List<Branch> findByStatus(String status);
	Branch findByCodeAndStatus(String code, String status);
}
