package com.app.core.service;

import java.util.List;

import com.app.core.bo.ConfigPropertyParam;
import com.app.core.model.ConfigProperty;

public interface ConfigPropertyService extends CrudPagingService<ConfigProperty, ConfigPropertyParam> {
	public List<ConfigProperty> findByStatus(String status);
	public void refreshConfigProperty();
	public ConfigProperty findByKey(String key);
}
