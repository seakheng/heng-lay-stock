package com.app.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.core.model.ConfigProperty;

@Repository
public interface ConfigPropertyRepository extends CrudRepository<ConfigProperty, Long>, JpaSpecificationExecutor<ConfigProperty> {
	List<ConfigProperty> findByStatus(String status);
	ConfigProperty findByKey(String key);
}
