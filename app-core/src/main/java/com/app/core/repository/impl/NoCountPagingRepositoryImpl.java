package com.app.core.repository.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.app.core.repository.NoCountPagingRepository;

public class NoCountPagingRepositoryImpl<T, I extends Serializable> 
extends SimpleJpaRepository<T, I> 
implements NoCountPagingRepository<T, I> {

	private EntityManager entityManager;

	public NoCountPagingRepositoryImpl(JpaEntityInformation<T, ?> 
	entityInformation, EntityManager entityManager) {
		super(entityInformation, entityManager);
		this.entityManager = entityManager;
	}

	@Override
	public List<T> findAllNoCount(Specification<T> spec, PageRequest pageRequest) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(getDomainClass());
		Root<T> root = cq.from(getDomainClass());
		CriteriaQuery<T> select = cq.select(root);
		cq.where(spec.toPredicate(root, cq, cb));
		TypedQuery<T> tq = entityManager.createQuery(select);
		tq.setFirstResult((int) pageRequest.getOffset());
		tq.setMaxResults(pageRequest.getPageSize()+1);
		return tq.getResultList();
	}

}
