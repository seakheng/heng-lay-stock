package com.app.core.repository;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface NoCountPagingRepository<T, I extends Serializable> extends JpaRepository<T, I>{
	List<T> findAllNoCount(Specification<T> spec, PageRequest pageRequest);
}
