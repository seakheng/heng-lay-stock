package com.app.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.app.core.model.Branch;

@Repository
public interface BranchRepository extends CrudRepository<Branch, Long>, JpaSpecificationExecutor<Branch> {
	List<Branch> findByStatus(String status);
	Branch findByCodeAndStatus(String code, String status);
}
