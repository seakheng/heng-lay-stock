package com.app.core.bo;

import lombok.Data;

@Data
public class BranchParam {
	private String name;
}
