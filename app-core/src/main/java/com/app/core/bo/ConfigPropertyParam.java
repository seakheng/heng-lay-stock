package com.app.core.bo;

import lombok.Data;

@Data
public class ConfigPropertyParam {
	private String name;
	private String key;
}
