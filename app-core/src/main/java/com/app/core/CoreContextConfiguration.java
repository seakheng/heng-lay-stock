package com.app.core;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.app.core.repository.impl.NoCountPagingRepositoryImpl;

@Configuration
@EnableJpaRepositories(repositoryBaseClass = NoCountPagingRepositoryImpl.class)
public class CoreContextConfiguration {

}
