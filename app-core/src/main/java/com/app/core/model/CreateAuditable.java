package com.app.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class CreateAuditable<T> extends StatusAuditable<T>{
	@Column(name = "created_on", nullable = false, updatable = false)
	@CreatedDate
	@Getter @Setter
	private Date createdOn;

	@Column(name = "created_by", nullable = false, updatable = false, length = 100)
	@CreatedBy
	@Getter @Setter
	private String createdBy;
}
