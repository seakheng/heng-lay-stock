package com.app.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sys_config_property")
@EntityListeners(AuditingEntityListener.class)
public class ConfigProperty extends Auditable<String> implements Serializable{
	private static final long serialVersionUID = -1063282662318920688L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;

	@Getter @Setter
	@Column(name = "name", length = 100)
	@NotBlank(message =  ValidationMessage.NOT_BLANK)
	@Size(min = 1, max = 100, message = ValidationMessage.SIZE)
	private String name;

	@Getter @Setter
	@Column(name = "key", length = 100, unique = true)
	@NotBlank(message =  ValidationMessage.NOT_BLANK)
	private String key;

	@Getter @Setter
	@Column(name = "value", length = 255)
	@NotBlank(message =  ValidationMessage.NOT_BLANK)
	private String value;
	
	@Getter
	@Setter
	@Column(name = "description", columnDefinition = "TEXT")
	@Size(max = 255, message = ValidationMessage.MAX)
	private String desc;
}
