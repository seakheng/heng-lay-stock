package com.app.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.Setter;


@EntityListeners(AuditingEntityListener.class)
@MappedSuperclass
public class BaseCrud extends Auditable<String> implements Serializable{

	private static final long serialVersionUID = -1063282662318920688L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter
	@Setter
	private Long id;

	@Getter
	@Setter
	@Column(unique = true, name = "code", length = 5, nullable = false, updatable = false)
	@Size(min = 1, max = 5, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String code;

	@Getter
	@Setter
	@Column(name = "name", length = 100, nullable = false)
	@Size(min = 1, max = 100, message = ValidationMessage.SIZE)
	@NotEmpty(message = ValidationMessage.NOT_EMPTY)
	private String name;

	@Getter
	@Setter
	@Column(name = "name_kh", length = 150, nullable = false)
	@Size(min = 1, max = 150, message = ValidationMessage.SIZE)
	private String nameKh;

	@Getter
	@Setter
	@Column(name = "description", length = 255)
	@Size(max = 255, message = ValidationMessage.MAX)
	private String desc;

	@Override
	public String toString() {
		return name;
	}

}
