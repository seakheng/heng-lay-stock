package com.app.core.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "sys_branch")
@EntityListeners(AuditingEntityListener.class)
public class Branch extends BaseCrud{
	private static final long serialVersionUID = -1063282662318920688L;
}
