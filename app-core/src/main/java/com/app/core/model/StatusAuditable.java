package com.app.core.model;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.constant.StatusEnum;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class StatusAuditable<T> {

	@Column(name = "status", nullable = false, length = 5)
	@Getter @Setter
	private String status = StatusEnum.STATUS_ACTIVE.getCode();

	public boolean isActive() {
		return StatusEnum.STATUS_ACTIVE.getCode().equals(this.getStatus());
	}

	public boolean isSuspended() {
		return StatusEnum.STATUS_SUSPEND.getCode().equals(this.getStatus());
	}

	public boolean isLocked() {
		return StatusEnum.STATUS_LOCKED.getCode().equals(this.getStatus());
	}

	public boolean isBlocked() {
		return StatusEnum.STATUS_BLOCK.getCode().equals(this.getStatus());
	}

	public boolean isDisabled() {
		return StatusEnum.STATUS_DISABLE.getCode().equals(this.getStatus());
	}

	public boolean isDeleted() {
		return StatusEnum.STATUS_DELETE.getCode().equals(this.getStatus());
	}

	public StatusEnum getStatusDesc() {
		return StatusEnum.getByCode().get(this.getStatus());
	}
	
	public String getStatusName() {
		return StatusEnum.getByCode().get(this.getStatus()).getName();
	}
	
	public String getStatusHtml() {
		StatusEnum s = StatusEnum.getByCode().get(this.getStatus());
		return "<span class='label "+s.getColor()+"'>" + s.getName() + "</span>";
	}
}
