package com.app.core.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class ModifiedAuditable<U> extends StatusAuditable<U>{

	@Column(name = "modified_on", nullable = true, insertable = false)
	@LastModifiedDate
	@Getter @Setter
	private Date modifiedOn;

	@Column(name = "modified_by", length = 100, nullable = true, insertable = false)
	@LastModifiedBy
	@Getter @Setter
	private String modifiedBy;
}