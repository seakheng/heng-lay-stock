package com.app.sample.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.app.core.repository.NoCountPagingRepository;
import com.app.sample.model.Sample;

@Repository
public interface SampleRepository extends NoCountPagingRepository<Sample, Long>, JpaSpecificationExecutor<Sample> {
	Optional<Sample> findByName(String name);
}
