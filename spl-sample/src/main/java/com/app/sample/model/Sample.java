package com.app.sample.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.app.core.model.Auditable;
import com.app.core.utils.ValidationMessage;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "sys_sample")
@EntityListeners(AuditingEntityListener.class)
public class Sample extends Auditable<String> implements Serializable {
	private static final long serialVersionUID = 2102331707751701264L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter @Setter
    private Long id;
	
	@Getter @Setter
    @Column(unique = true, name = "name")
	@NotBlank(message =  ValidationMessage.NOT_BLANK)
    private String name;
		
	@Getter @Setter
    @Column(name = "empty")
    private String empty;
    
    @Getter @Setter
    @Column(name = "note", length = 1024)
    private String note;
            
    Sample() {}
    
    public static Sample create(
    		final String name,
    		final String note) {
    	Sample sample = new Sample();
    	sample.setName(name);
    	sample.setNote(note);
    	return sample;
    }
}
