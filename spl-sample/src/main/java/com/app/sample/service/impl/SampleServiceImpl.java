package com.app.sample.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.app.sample.model.Sample;
import com.app.sample.repository.SampleRepository;
import com.app.sample.service.SampleService;

@Service("sampleService")
public class SampleServiceImpl implements SampleService {
	@Autowired
	SampleRepository repo;
	
	@Override
	public Optional<Sample> find(final Long id) {
		return repo.findById(id);
	}
	
	@Override
	public Optional<Sample> findByName(final String name) {
		return repo.findByName(name);
	}

	@Override
	public Sample save(Sample sample) {
		return repo.save(sample);
	}
	
	@Override
	public Iterable<Sample> find() {
		return repo.findAll();
		
	}

	@Override
	public void remove(Sample sample) {
		repo.delete(sample);
	}

	@Override
	public Page<Sample> findAll(Sample sample, PageRequest pageRequest) {
		return repo.findAll(spec(sample), pageRequest);
	}
	
	@Override
	public List<Sample> findAllNoCount(Sample sample, PageRequest pageRequest) {
		return repo.findAllNoCount(spec(sample), pageRequest);
	} 
	
	@Override
	public long count(Sample sample) {
		return repo.count(spec(sample));
	}
	
	public Specification<Sample> spec(Sample sample) {
		return (root, query, cb) -> {
			List<Predicate> predicates = new ArrayList<>();
			if(!StringUtils.isEmpty(sample.getName())) {
				predicates.add(cb.like(root.get("name"), "%"+sample.getName()+"%"));
			}
			if(!StringUtils.isEmpty(sample.getNote())) {
				predicates.add(cb.like(root.get("note"), "%"+sample.getNote()+"%"));
			}
			return cb.and(predicates.toArray(new Predicate[predicates.size()]));
		};
	}

	@Override
	public void delete(Sample model) {
		repo.delete(model);
		
	}
}
