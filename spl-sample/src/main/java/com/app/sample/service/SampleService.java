package com.app.sample.service;

import java.util.Optional;

import com.app.core.service.CrudService;
import com.app.core.service.PagingNoCountService;
import com.app.sample.model.Sample;

public interface SampleService extends CrudService<Sample>, PagingNoCountService<Sample, Sample>  {
	Optional<Sample> find(Long id);
	Optional<Sample> findByName(String name);
	Iterable<Sample> find();
	void remove(Sample sample);
}
