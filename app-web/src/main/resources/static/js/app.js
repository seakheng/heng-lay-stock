var myCodeMirrors = {};
var preventMenu = false;
var preventMessage = '';
var stompClient = null;
var queueSound = []
var isPlayingSound = false;
$(document).ready(function() {
	$('body').addClass('skin-blue sidebar-mini wysihtml5-supported iceblue_c');
	$('body').addClass(localStorage.getItem('sidebar-collapse'));
	$('.dialog img').on('load', function() {
		resizeImage();
	});
	$(window).resize(function() {
		setTimeout(function() {
			resizeImage();
		}, 1000);
	});
});

$('body').on('expanded.pushMenu collapsed.pushMenu', function(e) {
	var collape = localStorage.getItem('sidebar-collapse');
	if (collape === 'sidebar-collapse')
		localStorage.setItem('sidebar-collapse', '');
	else
		localStorage.setItem('sidebar-collapse', 'sidebar-collapse');
});

function changeClientUrl(urlPath) {
	window.history.pushState({}, "", urlPath);
	zkscript();
}

zk.afterMount(function() {
	zkscript();
});

function zkscript() {
	var previousUrl = window.location.pathname;
	window.onbeforeunload = function(event) {
		if (preventMenu === true) {
			var message = preventMessage;
			if (typeof event == 'undefined') {
				event = window.event;
			}
			if (event) {
				event.returnValue = message;
			}
			return message;
		}
	}
	window.onpopstate = function(e) {
		var confirmed = true;
		if (preventMenu === true) {
			confirmed = confirm(preventMessage);
		}
		if (confirmed === true) {
			var widget = zk.Widget.$(jq('$listenerDiv'));
			var path = window.location.pathname;
			zAu.send(new zk.Event(widget, "onClientStateChange", {
				'pathname' : path
			}, {
				toServer : true
			}));
		} else {
			window.history.pushState({}, "", previousUrl);
		}
	}

	$('.dialog img').unbind().on('load', function() {
		resizeImage();
	});
	$('.menu-item').unbind().on('click', function() {
		menuClick($(this));
	});
}

function codeMirror(wgt) {
	codeMirrors(wgt, 'text/x-sql');
}

function codeMirrors(wgt, mode) {
	var myCodeMirror = CodeMirror.fromTextArea(wgt.$n(), {
		mode : mode,
		sqlMode : true,
		indentWithTabs : true,
		smartIndent : true,
		matchBrackets : true,
		lineNumbers : true,
		extraKeys : {
			"Ctrl-Space" : "autocomplete"
		}
	});

	myCodeMirror.on('blur', function() {
		var val = myCodeMirror.getValue();
		wgt.$n().value = val;
		wgt.fire('onChange', {
			value : val
		}, {
			toServer : true
		});
	});
	myCodeMirrors[wgt.$n().id] = myCodeMirror;
}

function updateCodeMirror(value, wgt) {
	if (!value) {
		value = "";
	}
	for ( var key in myCodeMirrors) {
		if (wgt.$n() && key === wgt.$n().id) {
			var codeMirror = myCodeMirrors[key];
			codeMirror.getDoc().setValue(value);
		}
	}
}

function preventRedirect(prevent, msg) {
	preventMenu = prevent;
	preventMessage = msg;
}

function resizeImage() {
	var img = $('.dialog img');
	img.addClass('invisible');
	var height = img.closest('.image').height();
	var width = img.closest('.image').width();
	img.css('max-width', width * 2 - 100 + 'px');
	img.css('margin-left', -1 * (img.width() / 2) + 'px');
	img.css('max-height', height * 2 - 100 + 'px');
	img.css('margin-top', -1 * (img.height() / 2) + 'px');
	img.css('margin-left', -1 * (img.width() / 2) + 'px');
	img.hide();
	img.removeClass('invisible');
	runEffect();

}

function runEffect() {
	$('.dialog img').show(300);
}

function hideImage() {
	$('.dialog img').hide(10);
	$('.dialog img').removeAttr("style")
}

function downloadImage() {
	var img = $('.dialog img');
	var filename = img.attr('title') + '.png';
	var url = img.attr('src');
	console.log(filename + url);
	download(filename, url);
}

function download(filename, url) {
	console.log(url);
	console.log(filename + url);
	if (navigator.msSaveBlob) { // IE 10+
		navigator.msSaveBlob(new Blob([ url ]), filename);
	} else {
		var element = document.createElement('a');
		element.setAttribute('href', url);
		element.setAttribute('download', filename);
		element.setAttribute('target', '_blank')
		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();
		document.body.removeChild(element);
	}
}

function addAttr(com, attr, value) {
	var widget = zk.Widget.$(jq($(this)));
	console.log(com);
	// $(this).setAttribute(attr, value);
}


function filterLink() {
	var datafilter = $('.filter-link').val();
	$.each($('.applink .applink-icon'), function(i, com) {
		var data = $(com).attr('data-filter');
		if (data.toLowerCase().indexOf(datafilter.toLowerCase()) >= 0) {
			$(com).parent().show();
		} else {
			$(com).parent().hide();
		}
	});
}

function filterMenu() {
	var datafilter = $('.sidebar-form input').val();
	$.each($('.sidebar-menu li'), function(i, com) {
		var name = $(com).find('.menu-name').html();
		$(com).removeClass('filter-found');
		if (name != undefined
				&& name.toLowerCase().indexOf(datafilter.toLowerCase()) < 0) {
			$(com).hide();
		} else {
			$(com).show();
			var p = $(com).parent().parent('li');
			if (p !== undefined) {
				p.show();
				if (datafilter !== '') {
					$(com).addClass('filter-found');
					if (!p.hasClass('active'))
						p.addClass('active menu-open open-filter');
				} else {
					if (p.hasClass('open-filter')) {
						p.removeClass('active menu-open open-filter');
					}
				}
			}
		}
	});
	if (!$('.enter')) {
		activeEnterMenus(0);
	}
}

function clearMenuFilter() {
	$('.sidebar-form input').val('');
	$('.enter').removeClass('enter');
	filterMenu();
}

function activeEnterMenus(i) {
	console.log("menu: " + i + ", length: "
			+ $('.filter-found:not(.header) > a').length);
	$('.enter').removeClass('enter');
	var menu = $('.filter-found:not(.header) > a')[i];
	console.log(menu);
	$(menu).addClass('enter');
}

function arrowDownEnterMenu() {
	var i = $('.filter-found:not(.header) > a').index($('.enter'));
	var count = $('.filter-found:not(.header) > a').length;
	if (i >= count - 1)
		i = -1;
	activeEnterMenus(i + 1);
}

function arrowUpEnterMenu() {
	var i = $('.filter-found:not(.header) > a').index($('.enter'));
	var count = $('.filter-found:not(.header) > a').length;
	if (i <= 0)
		i = count;
	activeEnterMenus(i - 1);
}

function arrowEnterMenu(event) {
	switch (event.keyCode) {
	case 40:
		arrowDownEnterMenu();
		break;
	case 38:
		arrowUpEnterMenu();
		break;
	}
}

function clickFilterMenu() {
	var menu = $('.filter-found:not(.header) > a.enter')[0];
	if (menu !== undefined) {
		menuClick($(menu));
	}
}

function activeEnterMenu() {
	if (!$('.enter')) {
		var menu = $('.filter-found:not(.header) > a')[0];
		$(menu).addClass('enter');
	}
}

function menuClick(menuitem) {
	var confirmed = true;
	if (preventMenu === true) {
		confirmed = confirm(preventMessage);
	}
	if (confirmed === true) {
		preventMenu = false;
		$('.menu-item').parents('li').removeClass('active menu-open');
		menuitem.parents('li').addClass("active");
		menuitem.parent('li.treeview').removeClass("menu-open");
		menuitem.parent('li').parents('li.treeview').addClass("menu-open");
		var code = menuitem.attr('menu-code');
		var widget = zk.Widget.$(jq('$listenerDiv'));
		if (code !== '') {
			zAu.send(new zk.Event(widget, "onMenuClick", {
				'code' : code
			}, {
				toServer : true
			}));
		}
	}
}

function clipboard(id) {
	var copyText = zk.Widget.$(jq(id));
	copyText.select();
	document.execCommand("copy");
}

function copyStringToClipboard(str) {
	var el = document.createElement('textarea');
	el.value = str;
	el.setAttribute('readonly', '');
	el.style = {
		position : 'absolute',
		left : '-9999px'
	};
	document.body.appendChild(el);
	el.select();
	document.execCommand('copy');
	document.body.removeChild(el);
}

zk.print = function(uuid, uri, cssuri) {
	if (uuid && uri) {
		var wgt = zk.Widget.$(uuid), body = document.body, ifr = jq('#zk_printframe');
		if (!ifr[0]) {
			jq(body)
					.append(
							'<iframe id="zk_printframe" name="zk_printframe"'
									+ ' style="width:0;height:0;border:0;position:fixed;"'
									+ '></iframe>');
			ifr = jq('#zk_printframe');
		}
		// wait form submit response then call print function
		// reference:
		// http://isometriks.com/jquery-ajax-form-submission-with-iframes
		ifr.unbind('load.ajaxsubmit').bind('load.ajaxsubmit', function() {
			var iw = ifr[0].contentWindow || ifr[0];
			iw.document.body.focus();
			iw.print();
		});

		jq(body).append(
				'<form id="zk_printform" action="' + uri
						+ '" method="post" target="zk_printframe"></form>');
		var form = jq('#zk_printform'), content = '<div style="width: '
				+ wgt.$n().offsetWidth + 'px">' + jq(wgt.$n())[0].outerHTML
				+ '</div>';
		// form.append(jq('<input/>').attr({name: 'printContent', value:
		// content}));
		if (cssuri) {
			form.append(jq('<input/>').attr({
				name : 'printStyle',
				value : cssuri
			}));
		}
		form.submit().remove();
	} else {
		window.print();
	}
}

function reload() {
	location.reload();
}