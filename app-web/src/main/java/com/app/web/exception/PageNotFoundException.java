package com.app.web.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Page Not Found")
public class PageNotFoundException extends Exception {
	private static final long serialVersionUID = 171191125065736017L;

}
