package com.app.web.startup;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.app.core.constant.StatusEnum;
import com.app.core.service.ConfigPropertyService;
import com.app.core.utils.Utils;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@Profile({ "prd", "dev", "staging", "uat" })
public class Startup {
	@Autowired
	private ConfigPropertyService configPropertyService;

	@Autowired
	private ApplicationContext appContext;

	@PostConstruct
	public void init() {
		log.info("Setting up config property...");

		configPropertyService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode()).forEach(each -> {
			Utils.configProperties(appContext, each.getKey(), each.getValue());
		});
	}
}
