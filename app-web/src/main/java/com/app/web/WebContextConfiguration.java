package com.app.web;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.app.core.repository.impl.NoCountPagingRepositoryImpl;

@Configuration
@EnableJpaRepositories(basePackages = "com.app", repositoryBaseClass = NoCountPagingRepositoryImpl.class)
public class WebContextConfiguration {
	
}
