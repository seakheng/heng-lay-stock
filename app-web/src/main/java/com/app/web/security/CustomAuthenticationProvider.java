package com.app.web.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import com.app.security.model.UserPrincipal;
import com.app.security.service.UserService;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {

	private static Logger logger = LoggerFactory.getLogger(CustomAuthenticationProvider.class);
		
	@Autowired
    private UserService userService;
		
	@Override
	public Authentication authenticate(Authentication authentication) {
		String username = authentication.getName();
		String password = authentication.getCredentials().toString();
		
		UserPrincipal user = (UserPrincipal) userService.loadUser(username.toLowerCase(), password);
        if (user == null || !user.isEnabled()) {
            throw new UsernameNotFoundException("User details not found with this username: " + username);
        }
        try {			
			Session sess = Sessions.getCurrent();
		    sess.setAttribute("userCredential", user.getUser());
			return new UsernamePasswordAuthenticationToken(username, password, getAuthorities());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return null;
		

	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
	
	private List<SimpleGrantedAuthority> getAuthorities() {
        List<SimpleGrantedAuthority> authList = new ArrayList<>();
        authList.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authList;
    }
}