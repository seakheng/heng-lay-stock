package com.app.web.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.app.security.model.User;
import com.app.security.service.UserService;

public class SecurityUtil {

	private SecurityUtil() {}

	private static Logger logger = LoggerFactory.getLogger(SecurityUtil.class);

	public static boolean isAllGranted(String authorities) {		
		return isAuthenticated(authorities);
	}

	private static boolean isAuthenticated(final String func) {
		try {
			return isGranted(func);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return false;
	}

	private static boolean isGranted(String permission) {
		UserCredentialService userCredentialService = (UserCredentialService) context.getBean("userCredentialService");
		User user = userCredentialService.getUserCredential();
		var per = "";
		var func = "";
		if(permission.contains(".")) {
			var str = permission.split("\\.");
			func = str[0];
			per = str[1];
		}
		UserService userService = (UserService) context.getBean("userService");
		return userService.isGranted(user.getUsername(), func, per);
	}

	private static boolean isAuthenticated() {
		return (getAuthenticate() != null && getAuthenticate().isAuthenticated()
				&& !(getAuthenticate() instanceof AnonymousAuthenticationToken));
	}

	public static String getAuthenticateName() {
		return (isAuthenticated())? getAuthenticate().getName(): "anonymousUser";
	}

	private static Authentication getAuthenticate() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	@Autowired
	static ApplicationContext context;

	public static void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}

	public static ApplicationContext getApplicationContext() {
		return context;
	}
}
