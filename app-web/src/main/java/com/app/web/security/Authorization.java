package com.app.web.security;

import java.lang.reflect.Field;
import java.util.Optional;

public class Authorization {
	private Authorization () {}

	public static final String CREATE = "C_create";
	public static final String READ = "R_read";
	public static final String UPDATE = "U_update";
	public static final String DELETE = "D_delete";
	public static final String EXPORT = "X_export";
	public static final String SET_INACTIVE = "U_setInactive";
	public static final String SET_ACTIVE = "U_setActive";

	public static Optional<String> toShortCommand(final String longCommand) throws IllegalAccessException {
		Field[] declaredFields = Authorization.class.getDeclaredFields();
		Optional<String> opt = Optional.empty();
		for (Field field : declaredFields) {
			var fullCommand = (String) field.get(new Authorization());
			if (java.lang.reflect.Modifier.isStatic(field.getModifiers()) && 
					fullCommand.split("_")[1].equals(longCommand)) {
				opt = Optional.of(fullCommand.split("_")[0]);
			}
		}
		return opt;
	} 
}
