package com.app.web.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import com.app.security.model.User;
import com.app.security.model.UserPrincipal;
import com.app.security.service.UserService;

@Service("userCredentialService")
public class UserCredentialService {

	@Autowired
	UserService userService;

	public User getUserCredential(){
		Session sess = Sessions.getCurrent();
		User cre = (User) sess.getAttribute("userCredential");
		if(cre==null){
			UserPrincipal up = (UserPrincipal) userService.loadUser("anonymous.user", "anonymous@123");
			cre = up.getUser();
			sess.setAttribute("userCredential",cre);
		}
		return cre;
	}
}