package com.app.web.vm.utils;

import java.util.Locale;

import org.zkoss.text.DateFormatInfo;

public class CustomDateformat implements DateFormatInfo{
		
	@Override
	public String getDateFormat(int style, Locale locale) {
		
		return "dd/MM/yyyy";
	}

	@Override
	public String getTimeFormat(int style, Locale locale) {
		return "HH:mm";
	}

	@Override
	public String getDateTimeFormat(int dateStyle, int timeStyle, Locale locale) {
		return "DD/MM/YYYY HH:mm";
	}

}
