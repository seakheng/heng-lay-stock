package com.app.web.vm.sample;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.web.vm.utils.CrudListNoCountingVM;
import com.app.sample.model.Sample;
import com.app.sample.service.SampleService;

public class Sample4VM extends CrudListNoCountingVM<Sample, Sample>{
	
	//region > Service
	@WireVariable
	private SampleService sampleService;
	//endregion
		
	//region > Command
	@Init
	public void init(@ExecutionArgParam("type") String type) {
		super.init();
		super.setService(sampleService);
		setParam(Sample.create(null, null));
		setActiveCode(type);
		setSelectedItem(Sample.create("", ""));
	}
	//endregion
	
	//region > Utils
	
	@Override 
	protected Sample param() {
		return Sample.create("", "");
	}
	
	@Override 
	protected Object service() {
		return sampleService;
	}
	//endregion
}
