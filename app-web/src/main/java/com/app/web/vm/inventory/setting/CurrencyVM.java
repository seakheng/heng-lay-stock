package com.app.web.vm.inventory.setting;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.inv.bo.CurrencyParam;
import com.app.inv.model.Currency;
import com.app.inv.service.CurrencyService;
import com.app.web.vm.utils.CrudListCountingVM;

public class CurrencyVM extends CrudListCountingVM<Currency, CurrencyParam>{

	@WireVariable
	private CurrencyService currencyService;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new Currency());
		this.getLst().addAll(this.getResultList());
	}

	@Override
	protected CurrencyParam param() {
		return new CurrencyParam();
	}

	@Override
	protected Object service() {
		return currencyService;
	}
}
