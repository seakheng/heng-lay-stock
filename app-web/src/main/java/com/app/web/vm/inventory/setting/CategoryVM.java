package com.app.web.vm.inventory.setting;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.inv.bo.CategoryParam;
import com.app.inv.model.Category;
import com.app.inv.service.CategoryService;
import com.app.web.vm.utils.CrudListCountingVM;

public class CategoryVM extends CrudListCountingVM<Category, CategoryParam>{

	@WireVariable
	private CategoryService categoryService;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new Category());
		this.getLst().addAll(this.getResultList());
	}

	@Override
	protected CategoryParam param() {
		return new CategoryParam();
	}

	@Override
	protected Object service() {
		return categoryService;
	}
}
