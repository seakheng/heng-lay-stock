package com.app.web.vm;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Window;

import com.app.web.security.UserCredentialService;
import com.app.web.vm.utils.AppHelper;
import com.app.web.vm.utils.ViewModel;
import com.app.security.model.User;

import lombok.Getter;
import lombok.Setter;

public class LoginVM extends ViewModel{

	private RequestCache requestCache = new HttpSessionRequestCache();
	private static Logger logger = LoggerFactory.getLogger(LoginVM.class);

	private static String FORCE_CHANGE_PWD_ZUL = "force-change-pwd.zul";

	@WireVariable
	private UserCredentialService userCredentialService;

	@NotNull(message = "Username cannot be empty.")
	@Getter
	@Setter
	private String username;

	@NotNull(message = "Password cannot be empty.")
	@Getter
	@Setter
	private String password;

	@Getter
	@Setter
	private String message;

	@Getter
	private String contextPath = Executions.getCurrent().getContextPath();

	@Init
	@Override
	public void init() {
		super.init();
		if (isAuthenticated()) {
			Executions.sendRedirect("/");
		} else {
			Sessions.getCurrent().removeAttribute("userCredential");
		}
	}

	@NotifyChange("message")
	@Command
	public void login() {
		HttpServletRequest request = (HttpServletRequest) Executions.getCurrent().getNativeRequest();
		HttpServletResponse response = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
		try {
			request.login(this.username.toLowerCase(), this.password);
			SavedRequest savedRequest = requestCache.getRequest(request, response);
			User user = userCredentialService.getUserCredential();
			if ("Y".equals(user.getForceChangePwd())) {
				Map<String, Object> arg = new HashMap<>();
				arg.put("user_id", user.getId());
				Sessions.getCurrent().removeAttribute("userCredential");
				Window window = (Window) Executions.createComponents(
						FORCE_CHANGE_PWD_ZUL, null, arg);
				window.doModal();
				return;
			}
			String path = AppHelper.APPLICATION_PATH + "/login.zul";
			if (savedRequest != null && !savedRequest.getRedirectUrl().equals(path)) {
				Executions.sendRedirect(savedRequest.getRedirectUrl());
			} else {
				Executions.sendRedirect("/");
			}

		} catch (Exception e) {

			if ("No entity found for query".equals(e.getMessage())) {
				message = "User does not exist.";
			} else {
				message = "Wrong Username or Password. Please Try again.";	
			}
		} 
	}

	public boolean isAuthenticated() {
		return (
				getAuthenticate() != null && 
				getAuthenticate().isAuthenticated() &&
				!(getAuthenticate() instanceof AnonymousAuthenticationToken) );
	}

	private Authentication getAuthenticate() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		LoginVM.logger = logger;
	}



}