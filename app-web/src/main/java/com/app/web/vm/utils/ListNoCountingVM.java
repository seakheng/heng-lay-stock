package com.app.web.vm.utils;

import org.springframework.data.domain.PageRequest;
import org.zkoss.bind.annotation.Command;
import org.zkoss.zul.ListModelList;

import com.app.core.service.PagingNoCountService;

import lombok.Getter;
import lombok.Setter;

public abstract class ListNoCountingVM<T, P> extends PagingVM<T, P> {
	
	//region > Fields
	@Setter
	private PagingNoCountService<T, P> service;

	@Getter @Setter
	private boolean next;

	protected ListModelList<T> listModel;
	//endregion

	//region > Properties
	public boolean isPrevious() {
		return getActivePage() > 0;
	}

	public ListModelList<T> getListModel() {
		if(listModel == null) {
			listModel = new ListModelList<>();
			search();
		}
		return listModel;
	}
	//endregion
	
	//region > command
	@Command
	public void lastPage() {
		long count = service.count(getParam());
		int lastpage = (int) (count-1)/getPageSize();
		setActivePage(lastpage);
		search();
	}

	@Command
	public void nextPage() {
		setActivePage(getActivePage()+1);
		search();
	}

	@Command
	public void previousPage() {
		setActivePage(getActivePage()-1);
		search();
	}

	@Command
	public void firstPage() {
		setActivePage(0);
		search();
	}

	@Override
	public void search() {
		listModel.clear();
		listModel.addAll(service.findAllNoCount(getParam(), PageRequest.of(getActivePage(), getPageSize())));
		setNext(listModel.size()>getPageSize());
		if(isNext()) {
			listModel.remove(listModel.size()-1);
		}
		postNotifyChange(this, new String[] {"next", "previous", "activePage"});
	}

	//endregion

}
