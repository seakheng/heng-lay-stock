package com.app.web.vm.admin;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.app.core.constant.StatusEnum;
import com.app.security.bo.FunctionParam;
import com.app.security.model.Function;
import com.app.security.service.FunctionService;
import com.app.web.vm.utils.CrudListCountingVM;

import lombok.Getter;
import lombok.Setter;

public class FunctionVM extends CrudListCountingVM<Function, FunctionParam> {

	@WireVariable
	private FunctionService functionService;

	@Getter
	@Setter
	private ListModelList<Function> functions;

	@Getter
	@Setter
	private ListModelList<Function> functionsParam;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new Function());
		this.getLst().addAll(this.getResultList());
		this.initFunctions();

	}

	private void initFunctions() {
		this.setFunctions(new ListModelList<Function>(functionService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode())));
		this.setFunctionsParam(new ListModelList<Function>(this.getFunctions()));
	}

	@Override
	protected FunctionParam param() {
		return new FunctionParam();
	}

	@Override
	protected Object service() {
		return functionService;
	}
}
