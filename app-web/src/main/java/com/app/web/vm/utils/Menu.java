package com.app.web.vm.utils;

import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.codec.binary.StringUtils;

import com.app.security.model.Function;

import lombok.Getter;
import lombok.Setter;

public class Menu implements Comparable<Menu> {
		
	@Getter
	private SortedSet<Menu> menus = new TreeSet<>();
	
	@Getter @Setter
	private String name;
	@Getter @Setter
	private String code;
	@Getter @Setter
	private String path;
	@Setter
	private boolean active;
	@Getter @Setter
	private String icon;
	@Getter @Setter
	private int level=0;
	@Getter @Setter
	private int sequence;
	@Getter @Setter
	private String menuName;
	@Getter @Setter
	private String fullName;
	
	public boolean isActive() {
		return active || isSubmenuActive();
	}
	
	public boolean isSubmenuActive() {
		if(menus.isEmpty()) return false;
		for(Menu m:menus) {
			if(m.isActive()) return true;
		}
		return false;
	}

	public boolean add(Function func, String page) {
		if(!StringUtils.equals("Y", func.getMenu())) return false;
		var menu = new Menu();
		menu.setPath(func.getPath());
		menu.setCode(func.getCode());
		menu.setName(func.getName());
		menu.setIcon(func.getIcon());
		menu.setLevel(func.getLevel());
		menu.setSequence(func.getSequence());
		menu.setMenuName(func.getMenu());
		menu.setFullName(func.toString());
		if((menu.getLevel() == 1 && level == 0) || (StringUtils.equals(code, func.getParent().getCode()) && menu.getLevel() == level + 1)) {
			menu.setActive(StringUtils.equals(page, menu.getCode()));
			if(menu.isActive()) setActive(true);
			menus.add(menu);
			return true;
		}
		for(Menu m: menus) {
			if(m.add(func, page)) {
				return true;
			}
		}
		return false;
	}
	
	private boolean changeActive(String activeCode) {
		setActive(false);
		if(StringUtils.equals(getCode(), activeCode)) {
			setActive(true);
			return true;
		}
		for(Menu m: getMenus()) {
			var tm = m.changeActive(activeCode);
			if(tm) {
				setActive(true);
				return tm;
			}
		}
		return false;
	}
	
	public void setActiveMenu(String activeMenu) {
		changeInactive();
		changeActive(activeMenu);
	}
	
	private boolean changeInactive() {
		setActive(false);
		for(Menu m: getMenus()) {
			m.changeInactive();
		}
		return true;
	}
		
	public Menu getActiveMenu() {
		Menu activeMenu = isActive()?this:null;
		for(Menu m: menus) {
			if(m.isActive()) {
				activeMenu = m.getActiveMenu();
			}
		}
		return activeMenu;
	}
		
	public String getBreadcrumb(){
    	if(!isActive()) 
    		return "";
        StringBuilder str = new StringBuilder();
        str.append("<section class='content-header'>");
        str.append("<h1>");
        str.append("<i class='fa fa-"+getActiveMenu().getIcon()+"'></i> ");
        str.append(getActiveMenu().getName());
        str.append("</h1>");
        str.append("<ol class='breadcrumb'>");
        str.append("<li><a href='/'><i class='fa fa-dashboard'></i> Home</a></li>");
        str.append(getSubBreadcrumb());
        str.append("</ol>");
        str.append("</section>");
        return str.toString();
    }
	
	public String getSubBreadcrumb(){
    	boolean hasSubMenu = !(menus==null || menus.isEmpty());
    	StringBuilder str = new StringBuilder();
    	str.append("<li class='active'>");
    	str.append("<a menu-code='"+code.toLowerCase()+"' class='menu-item'>");
    	str.append(getName());
    	str.append("</a>");
    	str.append("</li>");
    	if(hasSubMenu) {
    		for(Menu menu: menus){
            	if(menu.isActive()) {
            		str.append(menu.getSubBreadcrumb());
            	}
            }
    	}
    	return str.toString();
    }
	
	public Menu getMenu(String code) {
		if(getCode()!=null && getCode().equalsIgnoreCase(code)) {
			return this;
		}
		for(Menu m: getMenus()) {
			var tm = m.getMenu(code);
			if(tm!=null) {
				return tm;
			}
		}
		return null;
	}

	@Override
	public int compareTo(Menu o) {
		return this.sequence - o.getSequence();
	}
}
