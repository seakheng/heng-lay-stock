package com.app.web.vm;

import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;

import com.app.web.vm.utils.ViewModel;

public class DashboardVM extends ViewModel {

	@Init
	public void init(@ExecutionArgParam("type") String type) {
		super.init();
		setActiveCode(type);
	}
}
