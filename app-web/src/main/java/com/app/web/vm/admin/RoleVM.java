package com.app.web.vm.admin;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.security.bo.RoleParam;
import com.app.security.model.Role;
import com.app.security.service.RoleService;
import com.app.web.vm.utils.CrudListCountingVM;

public class RoleVM extends CrudListCountingVM<Role, RoleParam> {

	@WireVariable
	private RoleService roleService;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new Role());
		this.getLst().addAll(this.getResultList());
	}

	@Override
	protected RoleParam param() {
		return new RoleParam();
	}

	@Override
	protected Object service() {
		return roleService;
	}
}
