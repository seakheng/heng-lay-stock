package com.app.web.vm.components;

import java.util.HashMap;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SidebarVM {


	@AfterCompose
	public void doAfterCompose(@ContextParam(ContextType.VIEW) Component view){
		Selectors.wireEventListeners(view, this);
	}


	@Listen("onClientStateChange=#listenerDiv")
	public void clientStateChange(Event event) {
		var arg = new HashMap();
		arg.put("data", event.getData());
		BindUtils.postGlobalCommand(null, null, "clientStateChange", arg);
	}

	@Listen("onMenuClick=#listenerDiv")
	public void menuClick(Event event) {
		JsonParser parser = new JsonParser();
		JsonObject jdata = parser.parse(event.getData().toString()).getAsJsonObject();
		var code = jdata.get("code").getAsString();
		var arg = new HashMap();
		arg.put("code", code);
		BindUtils.postGlobalCommand(null, null, "menuClick", arg);
	}
}
