package com.app.web.vm.utils;

import java.nio.file.AccessDeniedException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.Binder;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.DefaultCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.impl.BindContextImpl;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Window;

import com.app.web.security.Authorization;
import com.app.web.security.SecurityUtil;
import com.app.web.util.WebApplicationProperties;
import com.app.web.vm.components.ConfirmBoxVM;

import lombok.Getter;
import lombok.Setter;

@VariableResolver(DelegatingVariableResolver.class)
public class ViewModel {

	private static Logger logger = LoggerFactory.getLogger(ViewModel.class);

	@WireVariable("webApplicationProperties")
	@Getter
	private WebApplicationProperties app;

	@Getter @Setter
	protected String activeCode;

	@Init
	public void init() {
		Executions.getCurrent().getDesktop().getFirstPage().setTitle(app.getTitle());
	}

	@DefaultCommand
	public void defaultCommand(@ContextParam(ContextType.BINDER) Binder binder,
			@ContextParam(ContextType.EXECUTION) Execution exc,
			@ContextParam(ContextType.BIND_CONTEXT) BindContextImpl bindContext,
			@ContextParam(ContextType.COMMAND_NAME) String eventName)
			throws AccessDeniedException, IllegalAccessException {
		var opt = Authorization.toShortCommand(eventName);
		if (opt.isPresent() && SecurityUtil.isAllGranted(getActiveCode() + "." + opt.get())) {
			binder.postCommand(opt.get() + "_" + eventName, bindContext.getCommandArgs());
		} else {

			String arg = eventName + "(";
			if (bindContext.getCommandArgs() != null) {
				var i = 0;
				for (var a : bindContext.getCommandArgs().entrySet()) {
					if (i > 0 && i < bindContext.getBindingArgs().size())
						arg += ",";
					arg += a.getKey();
					i++;
				}
			}
			arg += ")";
			logger.info("Access denied: " + arg);
			throw new AccessDeniedException(Labels.getLabel("error.accessdenied.title"));
		}
	}

	public void showErrorMessage(String message) {
		Clients.showNotification(message, "error", null, "middle_center", 0);
	}

	public void showInfoMessage(String message) {
		Clients.showNotification(message, "info", null, "middle_center", 0);
	}

	public void showWarningMessage(String message) {
		Clients.showNotification(message, "warning", null, "middle_center", 0);
	}

	public void showErrorMessage(Throwable t) {
		showErrorMessage(t.getMessage());
	}

	public void postNotifyChange(final Object bean, final String property) {
		BindUtils.postNotifyChange(null, null, bean, property);
	}

	public void postNotifyChange(final Object bean, final String[] properties) {
		for (String property : properties) {
			BindUtils.postNotifyChange(null, null, bean, property);
		}
	}

	public void confirmMessage(String message, String onSuccess, String onCancel) {
		final Map<String, Object> map = new HashMap<>();
		map.put(ConfirmBoxVM.CONFIRM_MESSAGE, message);

		map.put(ConfirmBoxVM.ON_SUCCESS, onSuccess);
		map.put(ConfirmBoxVM.ON_CANCEL, onCancel);
		Window window = (Window) Executions.createComponents(ConfirmBoxVM.VIEW_PATH, null, map);
		window.doModal();
	}

	public Date getCurrentDate() {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}
}
