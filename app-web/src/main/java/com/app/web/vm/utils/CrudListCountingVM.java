package com.app.web.vm.utils;

import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Size;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

import com.app.web.security.Authorization;
import com.app.core.constant.StatusEnum;
import com.app.core.model.CreateAuditable;
import com.app.core.service.CrudService;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class CrudListCountingVM<T extends Object, P> extends PagingVM<T, P> {

	public static final String F_SELECTED_ITEM = "selectedItem";

	@Getter
	@Setter
	private T selectedItem;

	@SuppressWarnings("unchecked")
	public CrudService<T> getCrudService() {
		return ((CrudService<T>) this.service());
	}

	@Command(Authorization.CREATE)
	public void save() {
		log.debug("Intercept create record {}", this.getSelectedItem());
		try {
			getCrudService().save(this.getSelectedItem());
			this.clearForm();
			this.filter();
			this.showInfoMessage(Labels.getLabel("info.message.save"));
		} catch (Exception e) {
			log.error("Error while create record", e);
			showErrorMessage(e.getMessage());
		}
	}

	@Command(Authorization.UPDATE)
	public void update() {
		this.save();
	}

	@Command(Authorization.DELETE)
	public void delete() {
		log.debug("Intercept delete record {}", this.getSelectedItem());
		Messagebox.show(Labels.getLabel("confirm.message.delete"), "Execute?", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new EventListener<Event>() {
			@Override
			public void onEvent(final Event evt) throws InterruptedException {
				if (Messagebox.ON_YES.equals(evt.getName())) {
					try {
						getCrudService().delete(getSelectedItem());
						clearForm();
						filter();
						showInfoMessage(Labels.getLabel("info.message.delete"));
					} catch (Exception e) {
						log.error("Error while delete record", e);
						showErrorMessage(e.getMessage());
					}
				}
			}
		});
	}

	@SuppressWarnings("unchecked")
	@Command
	public void clearForm() {
		try {
			selectedItem = (T) selectedItem.getClass().getDeclaredConstructor().newInstance();
			this.postNotifyChange(this, F_SELECTED_ITEM);
			this.filter();
		} catch (Exception e) {
			log.error("Error while clear form", e);
		}
	}

	@SuppressWarnings("unchecked")
	@Command(Authorization.SET_INACTIVE)
	public void setInactive() {
		((CreateAuditable<String>) this.getSelectedItem()).setStatus(StatusEnum.STATUS_DISABLE.getCode());
		this.save();
	}

	@SuppressWarnings("unchecked")
	@Command(Authorization.SET_ACTIVE)
	public void setActive() {
		((CreateAuditable<String>) this.getSelectedItem()).setStatus(StatusEnum.STATUS_ACTIVE.getCode());
		this.save();
	}

	public org.zkoss.bind.Validator getFormValidator() {
		return new AbstractValidator() {
			public void validate(ValidationContext ctx) {
				ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
				Validator validator = vf.getValidator();
				var violations = validator.validate(getSelectedItem());
				for (var v : violations) {
					String message = v.getMessage();
					Path path = v.getPropertyPath();
					var cd = v.getConstraintDescriptor();
					var va = cd.getAnnotation();
					if (va instanceof Size) {
						Size s = (Size) va;
						addInvalidMessage(ctx, path.toString(), Labels.getLabel(message, new java.lang.Object[] {
								Labels.getLabel("field." + path.toString()), s.min(), s.max() }));
					} else {
						addInvalidMessage(ctx, path.toString(), Labels.getLabel(message, new java.lang.Object[] {
								Labels.getLabel("field." + path.toString()), v.getInvalidValue() }));
					}
				}
			}
		};
	}
}
