package com.app.web.vm.admin;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.app.core.constant.StatusEnum;
import com.app.security.bo.RoleFuncParam;
import com.app.security.model.Function;
import com.app.security.model.Role;
import com.app.security.model.RoleFunction;
import com.app.security.service.FunctionService;
import com.app.security.service.RoleFuncService;
import com.app.security.service.RoleService;
import com.app.web.vm.utils.CrudListCountingVM;

import lombok.Getter;
import lombok.Setter;

public class RoleFuncVM extends CrudListCountingVM<RoleFunction, RoleFuncParam> {

	@WireVariable
	private RoleFuncService roleFuncService;

	@WireVariable
	private RoleService roleService;

	@WireVariable
	private FunctionService functionService;

	@Getter
	@Setter
	private ListModelList<Role> roles;

	@Getter
	@Setter
	private ListModelList<Role> rolesParam;

	@Getter
	@Setter
	private ListModelList<Function> functions = new ListModelList<Function>();

	@Getter
	@Setter
	private ListModelList<Function> functionsParam;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new RoleFunction());
		this.getLst().addAll(this.getResultList());
		this.setRoles(new ListModelList<Role>(roleService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode())));
		this.setRolesParam(new ListModelList<Role>(this.getRoles()));
		this.setFunctionsParam(new ListModelList<Function>(functionService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode())));
	}

	private void initFunctions() {
		this.setFunctions(new ListModelList<Function>());
		if (this.getSelectedItem().getRole() != null)
			this.setFunctions(
					new ListModelList<Function>(functionService.findAllNotInRole(this.getSelectedItem().getRole())));
		this.postNotifyChange(this, "functions");
	}

	@Override
	protected RoleFuncParam param() {
		return new RoleFuncParam();
	}

	@Override
	protected Object service() {
		return roleFuncService;
	}

	@Override
	@Command
	public void clearForm() {
		super.clearForm();
		this.initFunctions();
	}

	@Command
	public void onChangeRole() {
		this.getSelectedItem().setFunction(null);
		this.postNotifyChange(this, "selectedItem");
		this.initFunctions();
	}

	@Command
	public void onView() {
		this.initFunctions();
	}
}
