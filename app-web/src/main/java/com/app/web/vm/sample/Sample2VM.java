package com.app.web.vm.sample;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.Validator;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.app.web.controller.SpringContext;
import com.app.web.vm.utils.PagingListModel;
import com.app.web.vm.utils.ViewModel;
import com.app.sample.model.Sample;
import com.app.sample.service.SampleService;

import lombok.Getter;
import lombok.Setter;

public class Sample2VM extends ViewModel{
	
	private static Logger logger = LoggerFactory.getLogger(Sample2VM.class);

	@Getter @Setter
	private Sample sample;
	
	@Getter
	private ListModelList<Sample> samples;
	
	@Getter
	private PagingListModel<Sample, Sample> sampleList;
	
	@WireVariable
	private SampleService sampleService;
	
	@Init
	public void init(@ExecutionArgParam("type") String type) {
		super.init();
		sample = Sample.create(null, null);
		setActiveCode(type);
		sampleList = PagingListModel.create(SpringContext.getAppContext(), sampleService, sample, 5);
	}
	
	@Command("secure_save")
	@NotifyChange({"sample", "samples"})
	public void save() {
		if(sample.getId()==null) {	
			sample = sampleService.save(sample);
			samples.add(sample);
		}
		else sampleService.save(sample);
	}
	
	@Command("secure_remove")
	@NotifyChange({"sample", "samples"})
	public void remove() {
		sampleService.remove(sample);
		samples.remove(sample);
		clean();
	}
	
	@Command
	@NotifyChange("sample")
	public void clean() {
		sample = Sample.create(null, null);
	}
	
	public Validator getFormValidator() {
	    return new AbstractValidator() {
	        public void validate(ValidationContext ctx) {
	        	if(StringUtils.isEmpty(sample.getName())) {
	        		addInvalidMessage(ctx, "name", "Name is required");
	        	}
	        }
	    };
	}

	public static Logger getLogger() {
		return logger;
	}

	public static void setLogger(Logger logger) {
		Sample2VM.logger = logger;
	}
	
	
}
