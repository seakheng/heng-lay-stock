package com.app.web.vm.utils;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.AbstractListModel;

import com.app.core.service.PagingService;

import lombok.Getter;
import lombok.Setter;

@VariableResolver(DelegatingVariableResolver.class)
public class PagingListModel<T, D> extends AbstractListModel<T> {
	private static final long serialVersionUID = 1785678326120276498L;

	@Getter @Setter
	private PagingService<T, D> pagingService;
	
	@Getter @Setter
	private boolean allowCache = true;
	
	@Getter @Setter
	private D domainObject;
	
	@Getter @Setter
	private int pagePerQuery = 1;
	
	@Getter @Setter
	private ApplicationContext context;
	
	Map<Integer, T> cache;
	
	@Getter @Setter
	private int pageSize = 10;
	
	@Getter @Setter
	private int activePage;
		
	@Getter @Setter
	private int size;
	
	private PagingListModel() {}
	
	public static <T, D> PagingListModel<T, D> create(ApplicationContext context, PagingService<T, D> pagingService, D domainObject, int pagePerQuery) {
		var slm = new PagingListModel<T, D>();
		slm.setContext(context);
		slm.setDomainObject(domainObject);
		slm.setPagePerQuery(pagePerQuery);
		Page<T> pageResult = pagingService.findAll(slm.getDomainObject(), PageRequest.of(slm.getActivePage()/slm.getPagePerQuery(), slm.getPageSize()*slm.getPagePerQuery()));
        slm.setSize((int) pageResult.getTotalElements());
        slm.setPagingService(pagingService);
        int indexKey = 0;
        for (T o : pageResult){
            slm.getCache().put(indexKey, o);
            indexKey++;
        }
        return slm;
	}
	
	@Override
	public T getElementAt(int index) {
		T target = getCache().get(index);
        if (target == null) {
        	if(!allowCache) getCache().clear();
            Page<T> pageResult = pagingService.findAll(domainObject, PageRequest.of(index/(pageSize*pagePerQuery), pageSize*pagePerQuery));
            int indexKey = index;
            for (T o : pageResult ){
                getCache().put(indexKey, o);
                indexKey++;
            }
        } else {
            return target;
        }

        target = getCache().get(index);
        if (target == null) {
            throw new RuntimeException("Element at index " + index + " cannot be found in the database.");
        } else {
            return target;
        }
	}
	
	private Map<Integer, T> getCache() {
		if(cache==null) {
			cache = new HashMap<>();
		}
        return cache;
    }
	
	public void updateAt(T object, int index) {
		cache.put(index, object);
	}
}
