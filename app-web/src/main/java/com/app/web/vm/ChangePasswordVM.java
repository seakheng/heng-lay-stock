package com.app.web.vm;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.web.security.UserCredentialService;
import com.app.web.vm.utils.ViewModel;
import com.app.core.crypto.Crypto;
import com.app.security.model.User;
import com.app.security.service.UserService;

import lombok.Getter;
import lombok.Setter;

public class ChangePasswordVM extends ViewModel {

	@WireVariable
	private UserCredentialService userCredentialService;

	@WireVariable
	private UserService userService;

	@WireVariable
	private Crypto crypto;

	@Getter
	@Setter
	private String oldPassword;

	@Getter
	@Setter
	private String password;

	@Getter
	@Setter
	private String confirmPassword;

	@Getter
	@Setter
	private String message;

	@Command
	@NotifyChange("message")
	public void save() {
		if(!this.getPassword().equals(this.getConfirmPassword())) {
			message = "The password and confirm password do not match.";
			return;
		}
		User user = userCredentialService.getUserCredential();
		String encryptPwd = crypto.encryptWithBase64(this.getOldPassword());
		System.err.println(encryptPwd);
		if (!user.getPassword().equals(encryptPwd)) {
			message = "The old password do not match.";
			return;
		}
		//Change Password
		userService.changePassword(user.getId(), this.getPassword());
		// Redirect to login page
		Sessions.getCurrent().invalidate();
		Executions.sendRedirect("/login");
	}

}
