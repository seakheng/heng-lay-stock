package com.app.web.vm;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.web.vm.utils.ViewModel;
import com.app.security.service.UserService;

import lombok.Getter;
import lombok.Setter;

public class ForceChangePasswordVM extends ViewModel {

	@WireVariable
	private UserService userService;

	@Getter
	@Setter
	private String password;

	@Getter
	@Setter
	private String confirmPassword;

	@Getter
	@Setter
	private String message;

	@Getter
	@Setter
	private Long userId;

	@Init
	@Override
	public void init() {
		userId = (Long) Executions.getCurrent().getArg().get("user_id");
	}

	@Command
	@NotifyChange("message")
	public void save() {
		if(!this.getPassword().equals(this.getConfirmPassword())) {
			message = "The password do not match.";
			return;
		}
		//Change Password
		userService.changePassword(userId, this.getPassword());
		this.onCloseWindow();
	}

	@Command
	public void onCloseWindow() {
		Sessions.getCurrent().invalidate();
		Executions.sendRedirect("/login");
	}

}
