package com.app.web.vm.utils;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zul.ListModelList;

import com.app.core.service.PagingService;

import lombok.Getter;
import lombok.Setter;

public abstract class PagingVM<T, P> extends ViewModel {

	private ListModelList<T> lst;
	private P param;
	@Getter
	@Setter
	private long totalSize;
	@Getter
	@Setter
	private int pageSize;;
	@Getter
	@Setter
	private int activePage = 0;
	@Getter
	@Setter
	private Sort sort;

	@Init
	public void init() {
		this.setSort(Sort.by(Sort.Direction.DESC, "id"));
		this.setPageSize(Integer.valueOf(this.getApp().getPageSize()));
	}

	@SuppressWarnings("unchecked")
	public PagingService<T, P> getPagingService() {
		return ((PagingService<T, P>) this.service());
	}

	public P getParam() {
		if (param == null) {
			param = this.param();
		}
		return param;
	}

	public void setParam(P param) {
		this.param = param;
	}

	public ListModelList<T> getLst() {
		if (lst == null) {
			lst = new ListModelList<>();
		}
		return lst;
	}

	public void setLst(ListModelList<T> lst) {
		this.lst = lst;
	}

	public List<T> getResultList() {
		return getPage().getContent();
	}

	public Page<T> getPage() {
		Page<T> page;
		if (this.getSort() != null) {
			page = getPagingService().findAll(this.getParam(),
					PageRequest.of(this.getActivePage(), this.getPageSize(), this.getSort()));
		} else {
			page = getPagingService().findAll(this.getParam(),
					PageRequest.of(this.getActivePage(), this.getPageSize()));
		}
		this.setTotalSize(page.getTotalElements());
		return page;
	}

	@Command
	public void onPaging() {
		search();
		postNotifyChange(this, new String[] { "lst" });
	}

	@Command
	public void search() {
		getLst().clear();
		getLst().addAll(this.getResultList());
		postNotifyChange(this, new String[] { "lst", "activePage", "totalSize" });
	}

	@Command()
	public void clear() {
		this.setParam(null);
		setActivePage(0);
		search();
		postNotifyChange(this, new String[] { "lst", "param", "activePage", "totalSize" });
	}

	@Command
	public void changePageSize(@BindingParam("size") int size) {
		setPageSize(size);
		search();
		postNotifyChange(this, new String[] { "pageSize" });
	}

	@Command
	public void filter() {
		setActivePage(0);
		search();
	}

	protected abstract P param();

	protected abstract Object service();
}
