package com.app.web.vm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.HandlerMapping;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.web.Attributes;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Include;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Window;

import com.app.security.model.Function;
import com.app.security.model.User;
import com.app.security.service.UserService;
import com.app.web.controller.SpringContext;
import com.app.web.exception.PageNotFoundException;
import com.app.web.security.SecurityUtil;
import com.app.web.security.UserCredentialService;
import com.app.web.vm.utils.Menu;
import com.app.web.vm.utils.ViewModel;
import com.google.gson.JsonParser;

import lombok.Getter;
import lombok.Setter;

public class MainVM extends ViewModel {
	@WireVariable
	private UserCredentialService userCredentialService;

	@WireVariable
	private UserService userService;

	@Getter
	@Setter
	private String systemName;

	@Getter
	@Setter
	private String miniLogo;

	@Getter
	@Setter
	private boolean userAD = false;

	@Getter
	private User user;

	private Menu menu;

	@Getter
	private String viewPath;

	@Getter
	private String lang;

	@Wire
	private Tabbox openedBox;
	@Wire
	private Tabs openedTabs;
	@Wire
	private Tabpanels openedPanels;

	private static final String BC1 = "components/breadscrum.zul";
	private static final String BC2 = "components/breadscrum2.zul";
	private String breadscrum = BC1;
	private String CHANGE_PASSWORD_ZUL = "change-pwd.zul";

	public String getBreadscrum() {
		breadscrum = BC1.equals(breadscrum) ? BC2 : BC1;
		return breadscrum;
	}

	@Init
	@Override
	public void init() {
		Session session = Sessions.getCurrent();
		Executions.getCurrent().getDesktop().getFirstPage().setTitle(getApp().getTitle());
		session.removeAttribute("page");
		user = userService.findByUsername(userCredentialService.getUserCredential().getUsername());
		// Init current user functions
		menu = null;
		SecurityUtil.setApplicationContext(SpringContext.getAppContext());
		ServletRequest request = (ServletRequest) Executions.getCurrent().getNativeRequest();	
		HashMap<?, ?> pathVariables = (HashMap<?, ?>) request
				.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
		String page = (String) pathVariables.get("page");

		lang = StringUtils.isEmpty(request.getParameter("lang")) ? "en" : request.getParameter("lang");	
		Locale prefer_locale = org.zkoss.util.Locales.getLocale(lang);
		session.setAttribute(Attributes.PREFERRED_LOCALE, prefer_locale);
		session.setAttribute("lang", lang);
		org.zkoss.util.Locales.setThreadLocal(org.zkoss.util.Locales.getLocale(lang));

		setActiveCode(StringUtils.isEmpty(page) ? "home" : page);
	}

	@AfterCompose
	public void doAfterCompose(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireEventListeners(view, this);
		Selectors.wireComponents(view, this, false);

		changeActivePage(getActiveCode());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void appendTab(Menu menu) {
		for(int i = 0; i < openedPanels.getChildren().size(); i++) {
			if (openedPanels.getChildren().get(i).getId().equals(menu.getCode())) {
				openedBox.setSelectedIndex(i);
				return;
			}
		}

		Tab tab = new Tab(menu.getName());
		tab.addEventListener(Events.ON_CLICK, new EventListener(){
			private String code = menu.getCode();
			private String vp = menu.getPath();

			@Override
			public void onEvent(Event arg0) throws Exception {
				activeCode = code;
				viewPath = vp;
				changeClientUrl(code);
			}
		});

		tab.addEventListener(Events.ON_CLOSE, new EventListener(){
			private String code = menu.getCode();

			@Override
			public void onEvent(Event arg0) throws Exception {
				int tabSize = openedTabs.getChildren().size();
				if (tabSize > 1 && activeCode.equals(code)) {
					int index = tabSize - 1;
					Component com = openedPanels.getChildren().get(index);

					if (com.getId().equals(code))
						com = openedPanels.getChildren().get(index - 1);

					activeCode = com.getId();
					viewPath = ((Include) com.getChildren().get(0)).getSrc();
					openedBox.setSelectedIndex(index);
					changeClientUrl(activeCode);
				}

				if (tabSize == 1)
					changeClientUrl("");

			}
		});

		tab.setTooltiptext(menu.getFullName());
		tab.setClosable(true);
		openedTabs.appendChild(tab);

		Executions.getCurrent().setAttribute("type", menu.getCode());
		Tabpanel tp = new Tabpanel();
		tp.setId(menu.getCode());
		tp.appendChild(new Include(menu.getPath()));
		openedPanels.appendChild(tp);

		openedBox.setSelectedIndex(openedTabs.getChildren().size() - 1);
	}

	private void changeActivePage(String activePage) {
		activePage = StringUtils.isEmpty(activePage) ? "home" : activePage;
		Menu m = getMenu().getMenu(activePage);
		if (m != null) {
			viewPath = m.getPath();
		}

		setActiveCode(activePage);

		menu.setActiveMenu(activePage);
		if (StringUtils.isEmpty(viewPath) || !userService.isGranted(user.getUsername(), activePage, "R")) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, Labels.getLabel("error._404.title"),
					new PageNotFoundException());
		}
		appendTab(m);
		postNotifyChange(this, new String[] { "viewPath", "breadscrum", "menu", "activeCode"});
	}

	@Command
	public void logout() {
		HttpServletResponse response = (HttpServletResponse) Executions.getCurrent().getNativeResponse();
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("employeeCredential");
		sess.removeAttribute("userCredential");
		Executions.sendRedirect(response.encodeRedirectURL("/logout"));
		Executions.getCurrent().setVoided(true);
	}

	@Command
	public void changePassword() {
		Window window = (Window) Executions.createComponents(CHANGE_PASSWORD_ZUL, null, null);
		window.doModal();
	}

	public boolean isAuthenticated() {
		return (getAuthenticate() != null && getAuthenticate().isAuthenticated()
				&& !(getAuthenticate() instanceof AnonymousAuthenticationToken));
	}

	public String getAuthenticateName() {
		return (isAuthenticated()) ? getAuthenticate().getName() : "";
	}

	private Authentication getAuthenticate() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public Menu getMenu() {
		if (menu == null) {
			generateMenu(getActiveCode());
		}
		return menu;

	}

	public void generateMenu(String code) {
		menu = new Menu();
		List<Function> temp = new ArrayList<>();
		for (Function func : userService.getUserFunction(user)) {
			if (!menu.add(func, code) && func.isActive()) {
				temp.add(func);
			}
		}
		int tempSize = temp.size();
		while (!temp.isEmpty()) {
			List<Function> fs = new ArrayList<>();
			for (Function f : temp)
				if (menu.add(f, code)) {
					fs.add(f);
				}
			temp.removeAll(fs);
			if (temp.size() == tempSize) {
				break;
			}
			tempSize = temp.size();
		}
	}

	@GlobalCommand
	public void menuClick(@BindingParam("code") String code) throws InterruptedException {
		changeActivePage(code);
		changeClientUrl(code);
	}

	@GlobalCommand
	public void clientStateChange(@BindingParam("data") String data) {
		String rootPath = Executions.getCurrent().getContextPath();
		var parser = new JsonParser();
		var jdata = parser.parse(data).getAsJsonObject();
		var pathname = jdata.get("pathname").getAsString().replace(rootPath, "").replace("/", "");
		changeActivePage(pathname);
		Clients.evalJavaScript("zkscript()");
	}

	@Command
	public void onRefreshTab() {
		for(int i = 0; i < openedPanels.getChildren().size(); i++) {
			Component com = openedPanels.getChildren().get(i);
			if (com.getId().equals(activeCode)) {
				com.getChildren().clear();
				Executions.getCurrent().setAttribute("type", activeCode);
				com.appendChild(new Include(viewPath));
				return;
			}
		}
	}

	private void changeClientUrl(String code) {
		String rootPath = Executions.getCurrent().getContextPath();
		Clients.evalJavaScript("changeClientUrl('" + rootPath + "/" + code + "')");
	}
}
