package com.app.web.vm.components;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Window;

import com.app.web.vm.utils.ViewModel;

import lombok.Getter;
import lombok.Setter;

public class ConfirmBoxVM extends ViewModel{
	public static final String VIEW_PATH = "components/confirm-box.zul";
	public static final String ON_SUCCESS ="__onSuccess";
	public static final String ON_CANCEL ="__onCancel";
	public static final String CONFIRM_MESSAGE = "__message";
	
	@Wire("#confirm_dialog")
    private Window win;
		
	@Getter @Setter
	private String message;
	
	@Getter @Setter
	private String onSuccess;
	
	@Getter @Setter
	private String onCancel;
	
	@Init
    public void init(@ContextParam(ContextType.VIEW) Component view,
    		@ExecutionArgParam(CONFIRM_MESSAGE) final String message,
    		@ExecutionArgParam(ON_SUCCESS) final String onSuccess,
    		@ExecutionArgParam(ON_CANCEL) final String onCancel) {
		Selectors.wireComponents(view, this, false);
		this.onSuccess = onSuccess;
		this.onCancel = onCancel;
		this.message = message;
	}
	
	@Command
	public void onOk() {
		if(!StringUtils.isEmpty(onSuccess)) {
			BindUtils.postGlobalCommand(null, null, onSuccess, null);
		}
		win.detach();
	}
	
	@Command
	public void onCancel() {
		if(!StringUtils.isEmpty(onCancel)) {
			BindUtils.postGlobalCommand(null, null, onCancel, null);
		}
		win.detach();
	}
}
