package com.app.web.vm.admin;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.core.bo.ConfigPropertyParam;
import com.app.core.model.ConfigProperty;
import com.app.core.service.ConfigPropertyService;
import com.app.web.vm.utils.CrudListCountingVM;

public class ConfigPropertyVM extends CrudListCountingVM<ConfigProperty, ConfigPropertyParam> {

	@WireVariable
	private ConfigPropertyService configPropertyService;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new ConfigProperty());
		this.getLst().addAll(this.getResultList());
	}

	@Override
	protected ConfigPropertyParam param() {
		return new ConfigPropertyParam();
	}

	@Override
	protected Object service() {
		return configPropertyService;
	}

	@Override
	@Command
	public void clearForm() {
		super.clearForm();
	}
}
