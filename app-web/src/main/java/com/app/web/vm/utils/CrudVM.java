package com.app.web.vm.utils;

import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Size;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zul.Messagebox;

import com.app.web.security.Authorization;
import com.app.core.service.CrudService;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
public abstract class CrudVM<T> extends ViewModel{

	@Getter @Setter
	private T model;

	@SuppressWarnings("unchecked")
	public CrudService<T> getCrudService() {
		return ((CrudService<T>)this.service()); 
	}

	@Command(Authorization.CREATE)
	public void save() {
		log.debug("Intercept create record {}", this.getModel());
		try {
			getCrudService().save(this.getModel());
			this.setModel(null);
			this.postNotifyChange(this, "model");
			this.showInfoMessage(Labels.getLabel("info.message.save"));
		} catch (Exception e) {
			log.error("Error while create record", e);
			showErrorMessage(e.getMessage());
		}
	}

	@Command(Authorization.UPDATE)
	public void update() {
		save();
	}

	@Command(Authorization.DELETE)
	public void delete() {
		log.debug("Intercept delete record {}", this.getModel());
		Messagebox.show(Labels.getLabel("confirm.message.delete"), "Execute?", Messagebox.YES | Messagebox.NO,
				Messagebox.QUESTION, new EventListener<Event>() {
			@Override
			public void onEvent(final Event evt) throws InterruptedException {
				if (Messagebox.ON_YES.equals(evt.getName())) {
					try {
						getCrudService().delete(getModel());
						setModel(null);
						postNotifyChange(this, "model");
						showInfoMessage(Labels.getLabel("info.message.delete"));
					} catch (Exception e) {
						log.error("Error while delete record", e);
						showErrorMessage(e.getMessage());
					}
				}
			}
		});
	}

	@Command()
	public void clearForm() {
		this.setModel(null);
		this.postNotifyChange(this, "model");
	}

	public org.zkoss.bind.Validator getFormValidator() {
		return new AbstractValidator() {
			public void validate(ValidationContext ctx) {
				ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
				Validator validator = vf.getValidator();
				var violations = validator.validate(getModel());
				for(var v : violations) {
					String message = v.getMessage();
					Path path = v.getPropertyPath();
					var cd = v.getConstraintDescriptor();
					var va = cd.getAnnotation();
					if (va instanceof Size) {
						Size s = (Size) va;
						addInvalidMessage(ctx, 
								path.toString(), 
								Labels.getLabel(message, new java.lang.Object[] {Labels.getLabel("field."+path.toString()), s.min(), s.max()}));
					}
					else {
						addInvalidMessage(
								ctx, 
								path.toString(), 
								Labels.getLabel(message, new java.lang.Object[] {Labels.getLabel("field."+path.toString()), v.getInvalidValue()}));
					}
				}
			}
		};
	}

	protected abstract Object service();
}
