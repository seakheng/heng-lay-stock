package com.app.web.vm.admin;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.ListModelList;

import com.app.core.constant.StatusEnum;
import com.app.core.model.Branch;
import com.app.core.service.BranchService;
import com.app.security.bo.UserParam;
import com.app.security.model.Role;
import com.app.security.model.User;
import com.app.security.service.RoleService;
import com.app.security.service.UserService;
import com.app.web.vm.utils.CrudListCountingVM;

import lombok.Getter;
import lombok.Setter;

public class UserVM extends CrudListCountingVM<User, UserParam> {

	@WireVariable
	private UserService userService;

	@WireVariable
	private RoleService roleService;

	@WireVariable
	private BranchService branchService;

	@Getter
	@Setter
	private ListModelList<Branch> branches;

	@Getter
	@Setter
	private ListModelList<Role> roles;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new User());
		this.getLst().addAll(this.getResultList());
		this.setRoles(new ListModelList<Role>(roleService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode())));
		this.setBranches(new ListModelList<Branch>(branchService.findByStatus(StatusEnum.STATUS_ACTIVE.getCode())));
	}

	@Override
	protected UserParam param() {
		return new UserParam();
	}

	@Override
	protected Object service() {
		return userService;
	}

}
