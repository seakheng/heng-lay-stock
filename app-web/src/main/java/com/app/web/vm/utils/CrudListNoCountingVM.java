package com.app.web.vm.utils;

import javax.validation.Path;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.Size;

import org.zkoss.bind.ValidationContext;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.validator.AbstractValidator;
import org.zkoss.util.resource.Labels;

import com.app.core.service.CrudService;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class CrudListNoCountingVM<T extends Object, P> extends ListNoCountingVM<T, P> {

	//region > fields
	public static final String F_SELECTED_ITEM = "selectedItem";
	@Getter @Setter
	private T selectedItem;
	//endregion

	//region > commands
	@Command 
	public void save() {
		CrudService<T> service = (CrudService<T>) service();
		selectedItem = service.save(selectedItem);
		showInfoMessage("Saved");
	}

	@Command
	public void clearForm() {
		try {
			selectedItem = (T) selectedItem.getClass().getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
		postNotifyChange(this, F_SELECTED_ITEM);
	}
	//endregion

	//region > Utils
	public org.zkoss.bind.Validator getFormValidator() {
		return new AbstractValidator() {
			public void validate(ValidationContext ctx) {
				ValidatorFactory vf = Validation.buildDefaultValidatorFactory();
				Validator validator = vf.getValidator();
				var violations = validator.validate(getSelectedItem());
				for(var v : violations) {
					String message = v.getMessage();
					Path path = v.getPropertyPath();
					var cd = v.getConstraintDescriptor();
					var va = cd.getAnnotation();
					if (va instanceof Size) {
						Size s = (Size) va;
						addInvalidMessage(ctx, 
								path.toString(), 
								Labels.getLabel(message, new java.lang.Object[] {Labels.getLabel("field."+path.toString()), s.min(), s.max()}));
					}
					else {
						addInvalidMessage(
								ctx, 
								path.toString(), 
								Labels.getLabel(message, new java.lang.Object[] {Labels.getLabel("field."+path.toString()), v.getInvalidValue()}));
					}
				}
			}
		};
	}
	//endregionn
}
