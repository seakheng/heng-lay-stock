package com.app.web.vm.admin;

import org.zkoss.bind.annotation.ExecutionParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.core.bo.BranchParam;
import com.app.core.model.Branch;
import com.app.core.service.BranchService;
import com.app.web.vm.utils.CrudListCountingVM;

public class BranchVM extends CrudListCountingVM<Branch, BranchParam> {

	@WireVariable
	private BranchService branchService;

	@Init
	public void init(@ExecutionParam("type") String type) {
		super.init();
		setActiveCode(type);
		this.setSelectedItem(new Branch());
		this.getLst().addAll(this.getResultList());
	}

	@Override
	protected BranchParam param() {
		return new BranchParam();
	}

	@Override
	protected Object service() {
		return branchService;
	}
}
