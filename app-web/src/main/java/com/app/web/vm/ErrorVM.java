package com.app.web.vm;

import java.nio.file.AccessDeniedException;
import java.util.Map;

import org.springframework.security.web.firewall.RequestRejectedException;
import org.zkoss.bind.annotation.Init;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.UiException;
import org.zkoss.zk.ui.select.annotation.WireVariable;

import com.app.web.exception.PageNotFoundException;
import com.app.web.vm.utils.ViewModel;

import lombok.Getter;

public class ErrorVM extends ViewModel {

	@Getter
	private String title;

	@Getter
	private String message;

	@WireVariable
	private Map<String, Object> requestScope;

	@Init
	@Override
	public void init() {
		Exception ex = (Exception) requestScope.get("javax.servlet.error.exception");
		System.err.println(requestScope);
		title = Labels.getLabel("error._" + requestScope.get("javax.servlet.error.status_code").toString() + ".title");
		message = Labels
				.getLabel("error._" + requestScope.get("javax.servlet.error.status_code").toString() + ".message");
		if (ex instanceof RequestRejectedException) {
		} else if (ex instanceof AccessDeniedException) {
			title = Labels.getLabel("error.accessdenied.title");
			message = Labels.getLabel("error.accessdenied.message" + ex.getMessage());
		} else if (ex instanceof UiException) {
			if (ex.getMessage().contains(AccessDeniedException.class.getName())) {
				title = Labels.getLabel("error.accessdenied.title");
				message = Labels.getLabel("error.accessdenied.message");
			}
		} else if (ex.getCause() instanceof PageNotFoundException
				|| ex.getCause().getCause() instanceof PageNotFoundException) {
			title = Labels.getLabel("error._404.title");
			message = Labels.getLabel("error._404.message");
		}
	}
}
