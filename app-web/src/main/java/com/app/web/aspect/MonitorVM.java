package com.app.web.aspect;

import java.util.List;

import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.Desktop;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.util.Monitor;

public class MonitorVM implements Monitor {

	@Override
	public void sessionCreated(Session sess) {
		// nothing to do here
	}

	@Override
	public void sessionDestroyed(Session sess) {
		// nothing to do here
	}

	@Override
	public void desktopCreated(Desktop desktop) {
		// nothing to do here
	}

	@Override
	public void desktopDestroyed(Desktop desktop) {
		// nothing to do here
	}

	@Override
	public void beforeUpdate(Desktop desktop, List<AuRequest> requests) {
		for (var req : requests) {
			Executions.getCurrent().getSession().setAttribute("command", req.getCommand());
			Executions.getCurrent().getSession().setAttribute("commandRequest", requests);
		}
	}

	@Override
	public void afterUpdate(Desktop desktop) {
		// nothing to do here
	}

}
