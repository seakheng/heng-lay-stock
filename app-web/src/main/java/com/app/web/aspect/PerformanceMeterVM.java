package com.app.web.aspect;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.zk.au.AuRequest;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.PerformanceMeter;

import com.app.web.security.SecurityUtil;
import com.app.security.model.User;
import com.app.tracelog.model.AccessLog;
import com.app.tracelog.model.AccessLogData;
import com.app.tracelog.service.AccessLogService;

public class PerformanceMeterVM implements PerformanceMeter {

	Logger logger = LoggerFactory.getLogger(PerformanceMeterVM.class);
	private long startAtClient;
	private long recieveAtServer;
	/**
	 * list of events to trace log
	 */
	private static final String[] traceEvents = { "onMenuClick", "rmDesktop", "clickService", "initService" };

	@Override
	public void requestStartAtClient(String requestId, Execution exec, long time) {
		startAtClient = time;
	}

	@Override
	public void requestReceiveAtClient(String requestId, Execution exec, long time) {
		// do nothing here
	}

	@Override
	public void requestCompleteAtClient(String requestId, Execution exec, long time) {
		// do nothing here
	}

	@Override
	public void requestStartAtServer(String requestId, Execution exec, long time) {
		recieveAtServer = time;
	}

	@Override
	public void requestCompleteAtServer(String requestId, Execution exec, long time) {
		var completeAtServer = time;
		User user = (User) exec.getSession().getAttribute("userCredential");
		String page = (String) exec.getSession().getAttribute("page");
		var command = Sessions.getCurrent().getAttribute("command");
		if (Arrays.stream(traceEvents).anyMatch(each -> each.equals(command))) {

			AccessLog accessLog = AccessLog.create(requestId, command == null ? "" : command.toString(),
					new Date(startAtClient), new Date(recieveAtServer), new Date(completeAtServer),
					exec.getRemoteAddr(), exec.getUserAgent(), user != null ? "" + user.getId() : "");
			AccessLogData pageData = AccessLogData.create("page", page);
			accessLog.addData(pageData);

			generateLogData(exec, accessLog);

			saveLog(accessLog);
		}
		Sessions.getCurrent().removeAttribute("command");
	}

	private void generateLogData(Execution exec, AccessLog accessLog) {
		var requests = (List<AuRequest>) exec.getSession().getAttribute("commandRequest");
		if (requests != null) {
			for (var req : requests) {
				for (var data : req.getData().entrySet()) {
					AccessLogData accessLogData = AccessLogData.create(data.getKey(), data.getValue().toString());
					accessLog.addData(accessLogData);
				}
			}
		}
	}

	private void saveLog(AccessLog accessLog) {
		if (accessLogService == null)
			wireService();
		accessLogService.save(accessLog);
	}

	private AccessLogService accessLogService;

	public void wireService() {
		var context = SecurityUtil.getApplicationContext();
		accessLogService = (AccessLogService) context.getBean("accessLogService");
	}

}
