package com.app.web.util;

import java.util.Optional;

import org.springframework.data.domain.AuditorAware;

import com.app.web.security.SecurityUtil;

public class AuditorAwareImpl implements AuditorAware<String> {

	@Override
	public Optional<String> getCurrentAuditor() {
		return Optional.of(SecurityUtil.getAuthenticateName());
	}
	
}
