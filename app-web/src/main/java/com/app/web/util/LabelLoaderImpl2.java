package com.app.web.util;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.util.resource.impl.LabelLoaderImpl;

public class LabelLoaderImpl2 extends LabelLoaderImpl {	
	
	@Override
	public String getLabel(String key) {
		String value = null;
		try {
			value = super.getLabel(key);
		} catch (Exception e) {
			
		}
		return value;
	}
	
	@Override
	public Map<String, Object> getSegmentedLabels() {
		Map<String, Object> labels = new HashMap<>();
		try {
			labels.putAll(super.getSegmentedLabels());
		} catch (Exception e) {
			
		}
		return labels;
	}
}
