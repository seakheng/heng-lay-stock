package com.app.web.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@ConfigurationProperties(prefix = "app")
public class WebApplicationProperties {

	@Getter @Setter
	@Value("${app.small.logo:#{null}}")
	private String smallLogo = "";

	@Getter @Setter
	@Value("${app.large.logo:#{null}}")
	private String largeLogo = "";

	@Getter @Setter
	@Value("${app.name:#{null}}")
	private String name = "System Name";

	@Getter @Setter
	@Value("${app.short.name:#{null}}")
	private String shortName = "System Short Name";

	@Getter @Setter
	@Value("${app.title:#{null}}")
	private String title = "System Title";

	@Getter @Setter
	@Value("${app.version:#{null}}")
	private String version = "";

	@Getter @Setter
	@Value("${app.copyright:#{null}}")
	private String copyright = "";

	@Getter @Setter
	@Value("${app.date.format:#{null}}")
	private String dateFormat = "dd/MM/yyyy";

	@Getter @Setter
	@Value("${app.time.format:#{null}}")
	private String timeFormat = "HH:mm";

	@Getter @Setter
	@Value("${app.datetime.format:#{null}}")
	private String datetimeFormat = "dd/MM/yyyy HH:mm";

	@Getter @Setter
	@Value("${app.double.format:#{null}}")
	private String doubleFormat = "#,###.00";

	@Getter @Setter
	@Value("${app.integer.format:#{null}}")
	private String integerFormat = "#,###";

	@Getter @Setter
	@Value("${app.long.format:#{null}}")
	private String longFormat = "#,###";

	@Getter @Setter
	@Value("${app.page.size:#{null}}")
	private String pageSize = "10";

	@Getter @Setter
	@Value("${app.static.file.server:#{null}}")
	private String staticFileServer = "";
}
