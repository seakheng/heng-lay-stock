package com.app.web.util;

import java.util.Locale;

public class DateFormatInfo implements org.zkoss.text.DateFormatInfo {
    @Override
    public String getDateFormat(int style, Locale locale) {
        return "MMM dd, yyyy";
    }

    @Override
    public String getTimeFormat(int style, Locale locale) {
        return "HH:mm";
    }

    @Override
    public String getDateTimeFormat(int dateStyle, int timeStyle, Locale locale) {
        return "MMM dd, YYYY HH:mm";
    }
}
