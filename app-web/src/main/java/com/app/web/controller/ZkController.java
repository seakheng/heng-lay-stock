package com.app.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class ZkController {

	@Getter
	@Setter
	@Value("${app.static.file.server:#{null}}")
	private String staticFileServer = "";

	@GetMapping("/home")
	public String homePage() {
		return "home";
	}

	@GetMapping("/login")
	public String loginPage() {
		return "login";
	}

	@GetMapping("/error")
	public String secure() {
		return "components/error";
	}

	@GetMapping("/{page}")
	public String view(@PathVariable("page") String page) {
		return "home";
	}

	@GetMapping("/{page}/{type}/id-{id}")
	public String viewWithId(@PathVariable("page") String page, @PathVariable("type") String type,
			@PathVariable("id") String id) {
		return "home.zul?id=" + id + "&type=" + type;
	}
}
