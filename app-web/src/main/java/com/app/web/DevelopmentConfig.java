package com.app.web;

import java.lang.reflect.Field;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.zkoss.util.resource.Labels;
import org.zkoss.util.resource.impl.LabelLoaderImpl;


@Configuration
@Profile("h2dev")
public class DevelopmentConfig {
    private static Logger logger = LoggerFactory.getLogger(DevelopmentConfig.class);
    
    @PostConstruct
    public void initDevelopmentProperties() {
        resetLabelLoader();
    }
    
    private static void resetLabelLoader() {
		try {
			final Field field = Labels.class.getDeclaredField("_loader");
			field.setAccessible(true);
			field.set(null, new LabelLoaderImpl());
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}
