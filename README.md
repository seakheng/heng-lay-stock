#Starter

Adapted from https://github.com/zkoss/zkspringboot

## Setup

Prerequisites
  - OpenJdk 11
  - Maven 3.6.0
  - Eclipse 2018.9
  - [Lombok](https://projectlombok.org/download)
   
### Run Project

default `env` is `dev`, to change env use flag `-D`. e.g `-Dtest` `-Dprod` `-Ddev`

    mvn clean install
    
* Run Test

	mvn test -Dtest
	
* Run project

on web module run

    mvn spring-boot:run

System will start on default port `8080`
    
* Build for Production

on web module run 

    mvn clean install -Dprod -Dmaven.test.skip=true
    
### Labels

By default the property file must be placed under the resources/metainfo 
directory and named as zk-label_lang_CNTY.properties.  A properties file 
is a simple text file encoded in UTF-8[2]. The file contains a list of 
key=value pairs.

To display labels on zul file `${labels.key}`. To get labels on Java code 
`Labels.get(key)` 
[more](https://www.zkoss.org/wiki/ZK_Developer's_Reference/Internationalization/Labels)

### Authorization

Authorization is used to check if a user is allowed to perform some specific 
operation in the application.

#### Defining Permissions

A unique permission is defined for each operation that needs to be authorized. 
We need to define a permission before it is used. 
`com.app.web.security.Authorization` in order to define the permissions by 
combining short command provide by `Single Signon` and `Long Command` using on 
the system. An example authorization is shown below:

	public static final String EXECUTE = "exe_execute";
	public static final String COMMIT = "com_commit";
	public static final String PRE_EXECUTE = "pex_preExecute";
	
#### Checking Permissions

##### Using Security TagLib `ZUL`

Security Taglib attribute is the easiest and most common way of checking 
permissions on client side "`zul`". Consider two step shown below:

**Step 1**: Define security `taglib` on `ZUL` file as shown below:

	<?taglib uri="/WEB-INF/security.tld" prefix="sec"?>

**Step 2**: using `if`, `visible` or `disabled` attribute of `zul`'s 
tag as shown below:

    <a onClick="@command('execute')" if="${sec:isAllGranted(vm.activeCode.concat('.execute'))}"
			sclass="btn btn-danger">
		<n:i class="fa fa-remove">Execute</n:i>
	</a>

Button above show only if user has permission to execute command `execute` of current workspace.

##### Checking Permissions on server side `Command`

To check permission on `Command` we use anotation command same as normal 
command with `Authorization.Permissions`.

	@Command(Authorization.EXE_EXECUTE)
	public void execute() {}
	
**Note:** Permission check only perform when call `command` from `zul`. If command being call
internally filter can not trigger.

### Paging

### Audit Trail

### Validations

### Fixture Script

### Firefox Disable Printer Review

**Step 1**: In the address bar type `about:config` and press enter

**Step 2**: Find `print.always_print_silent`, `print.show_print_progress` and `dom.successive_dialog_time_limit`
then update shown below:

    print.always_print_silent = true
    print.show_print_progress = false
    dom.successive_dialog_time_limit = 0

### Display Service and TV

**Step 1**: Allow auto play video and allow sound on `Chrome` by go to `setting` -> `Site Settings` -> `Sound` then click on button `Add` on section `Allow` and then add url `https://qms.hkl.com.kh:8443`

**Step 2**: Create shortcut of `Chrome` and `Firefox` on desktop

**Step 3**: Edit target of each shortcut by select properties then add more config shown below:

	-kiosk --window-position=1024,0 https://qms.hkl.com.kh:8443/display-signage?brn={branch_code} //chrome
	-kiosk https://qms.hkl.com.kh:8443/display-service?brn={branch_code} //firefox
	
**Step 4**: Auto startup display (service and TV) by open `Run Command` then typing `shell:startup` and press enter. It will show startup directory and then copy shortcut of `Chrome` and `Firefox` from desktop paste into it.
 
More
  1. [ZK Book](http://books.zkoss.org/zk-mvvm-book/8.0/)

